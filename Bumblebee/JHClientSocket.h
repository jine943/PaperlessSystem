#ifndef _JH_SOCKET_CLIENT_
#define _JH_SOCKET_CLIENT_

#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")

#define MAX_CLIENT_COUNT  1000


class JHClientSocket : public CDialog
{
	DECLARE_DYNAMIC(JHClientSocket)
protected:
	SOCKET mh_socket;

	//const wchar_t *m_test_server_ip;
	const char *m_test_server_ip;
	char m_server_ip[24];
	unsigned short m_port;
	// 접속 유무를 알리는 flag 선언하여 접속이 된 경우에 소켓 기능 사용하게 하기

	CListBox *mp_list_box;

public:
	JHClientSocket(UINT nIDTemplate, CWnd* pParentWnd);
	~JHClientSocket();

	void SetSocketData(const char *parm_address, unsigned short parm_port);

	void Connect();

	char ProcessReadMessage(SOCKET parm_socket);
	char ProcessCloseMessage(SOCKET parm_socket);
	char ProcessConnectMessage(SOCKET parm_socket);
	
	char DestroySocket(SOCKET parm_socket);

	int SendFrameData(SOCKET parm_socket, unsigned char parm_message_id, void *parm_data, int parm_send_size);
	int SendFrameData(unsigned char parm_message_id, void *parm_data, int parm_send_size);
	char ReadBodyData(SOCKET parm_socket, int parm_size, char *parm_body_data);
	
	virtual void UserProcessReadData(unsigned char parm_message_id, unsigned int parm_data_size, void *parm_data){ }

	virtual void UserProcessAfterConnect(){}

	DECLARE_MESSAGE_MAP()
	LRESULT OnSocketProcess(WPARAM wParam, LPARAM lParam);

};


#endif