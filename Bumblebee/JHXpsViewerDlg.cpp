// JHXpsViewerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JH_BumBleBee.h"
#include "JHXpsViewerDlg.h"
#include "afxdialogex.h"


// JHXpsViewerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(JHXpsViewerDlg, CDialogEx)

JHXpsViewerDlg::JHXpsViewerDlg(CWnd* pParent) :CDialog(JHXpsViewerDlg::IDD, pParent)
{
	m_xps_page_count = 0;
	m_page_show_count = 0;
	m_is_animating = 1;
	m_user_page_index = 0;
	m_send_flag = 0;
	m_modify_flag = 0;
	m_is_clicked = 0;
	mp_mem_dc = NULL;
	mp_mem_bitmap = NULL;
	memset(m_change_arry, 0, MAX_FILE_COUNT);
	m_changed_page_count = 0;
}

JHXpsViewerDlg::~JHXpsViewerDlg()
{
}

void JHXpsViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_COLOR_LIST, m_color_list);
	DDX_Control(pDX, IDC_WIDTH_LIST, m_width_list);
}


BEGIN_MESSAGE_MAP(JHXpsViewerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_MODIFY_BTN, &JHXpsViewerDlg::OnBnClickedModifyBtn)
	ON_WM_DESTROY()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MODIFY_CANCEL_BTN, &JHXpsViewerDlg::OnBnClickedModifyCancelBtn)
	ON_BN_CLICKED(IDC_SEND_BUTTON, &JHXpsViewerDlg::OnClickedSendButton)
	ON_CBN_SELCHANGE(IDC_COLOR_LIST, &JHXpsViewerDlg::OnSelchangeColorList)
	ON_CBN_SELCHANGE(IDC_WIDTH_LIST, &JHXpsViewerDlg::OnSelchangeWidthList)
END_MESSAGE_MAP()

#define MAX_COLOR_COUNT   5
#define MAX_WIDTH_COUNT   3

// JHXpsViewerDlg 메시지 처리기입니다.

void JHXpsViewerDlg::SetFileName(wchar_t *parm_p_name, int parm_file_name_size)
{
	mp_xps_page_image[m_xps_page_count] = new CImage();

	// File 명을 맴버에 저장
	
	m_file_name_size[m_xps_page_count] = parm_file_name_size*2;
	wcscpy_s(m_send_file_name[m_xps_page_count], m_file_name_size[m_xps_page_count], parm_p_name);
	mp_xps_page_image[m_xps_page_count]->Load(parm_p_name);
	
	m_xps_page_count++;
	m_user_page_index = m_xps_page_count - 1;
	
	if (m_is_animating == 0){
		m_is_animating = 1;
		m_step_count = 0;
		SetTimer(2, 5, NULL);
	}
}

void JHXpsViewerDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if (m_modify_flag == 0){
		CRect r(0, 0, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight());
		mp_xps_page_image[m_user_page_index]->Draw(dc.GetSafeHdc(), r);
	}
	else {
		CBitmap *p_old_bitmap = mp_mem_dc->SelectObject(mp_mem_bitmap);
		dc.BitBlt(0, 0, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight(),
			mp_mem_dc, 0, 0, SRCCOPY);
		mp_mem_dc->SelectObject(p_old_bitmap);
	}
}


BOOL JHXpsViewerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	MoveWindow(CRect(mp_xps_page_image[0]->GetWidth()*(-1), 0, 0, mp_xps_page_image[0]->GetHeight()));

	SetTimer(1, 5, NULL);
	GetDlgItem(IDC_SEND_BUTTON)->ShowWindow(SW_HIDE);

	////////////////////추가부분///////////////////////
	GetDlgItem(IDC_COLOR_LIST)->ShowWindow(SW_HIDE);//콤보박스는 펜기능 활성화일때만 보이게
	GetDlgItem(IDC_WIDTH_LIST)->ShowWindow(SW_HIDE);//콤보박스는 펜기능 활성화일때만 보이게

	CString color_name[MAX_COLOR_COUNT] = { L"검정색", L"빨강색", L"초록색", L"파랑색", L"흰색" };
	COLORREF color_value[MAX_COLOR_COUNT] = {
		RGB(0, 0, 0), RGB(255, 0, 0), RGB(29, 219, 22), RGB(0, 0, 255), RGB(255, 255, 255)
	};

	for (int i = 0; i < MAX_COLOR_COUNT; i++){
		m_color_list.InsertString(i, color_name[i]);
		m_color_list.SetItemData(i, color_value[i]);
	}

	m_color_list.SetCurSel(0);

	CString width_name[MAX_WIDTH_COUNT] = { L"얇음", L"보통", L"굵음" };
	unsigned short int width_value[MAX_WIDTH_COUNT] = { 1, 2, 4 };

	for (int i = 0; i < MAX_WIDTH_COUNT; i++){
		m_width_list.InsertString(i, width_name[i]);
		m_width_list.SetItemData(i, width_value[i]);
	}

	m_width_list.SetCurSel(1);

	m_pen_color = color_value[0];//처음 기본색은 검정색
	m_pen_width = width_value[1];//처음 기본굵기는 보통
	m_pen.CreatePen(PS_SOLID, m_pen_width, m_pen_color);//처음 기본색은 검정색
	////////////////////추가부분///////////////////////

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void JHXpsViewerDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 1){
		MoveWindow(CRect(mp_xps_page_image[0]->GetWidth()*(-1) + m_step_count, 0, m_step_count, mp_xps_page_image[0]->GetHeight()));

		if (m_step_count < mp_xps_page_image[0]->GetWidth()) m_step_count += 20;
		else {
			KillTimer(1);
			m_page_show_count = 1;

			m_step_count = 0;

			if (m_page_show_count < m_xps_page_count){
				SetTimer(2, 5, NULL);
			}
			else {
				m_is_animating = 0;
			}
		}
	}
	else if (nIDEvent == 2){
		CClientDC dc(this);

		CRect r;
		if (m_step_count < mp_xps_page_image[m_page_show_count]->GetWidth()){
			r.SetRect(mp_xps_page_image[m_page_show_count]->GetWidth()*(-1) + m_step_count, 0,
				m_step_count, mp_xps_page_image[m_page_show_count]->GetHeight());
			m_step_count += 20;
			mp_xps_page_image[m_page_show_count]->Draw(dc.GetSafeHdc(), r);
		}
		else {
			r.SetRect(0, 0,
			mp_xps_page_image[m_page_show_count]->GetWidth(), mp_xps_page_image[m_page_show_count]->GetHeight());
			mp_xps_page_image[m_page_show_count]->Draw(dc.GetSafeHdc(), r);

			m_page_show_count++;
			m_step_count = 0;

			if (m_page_show_count < m_xps_page_count){

			} else {
				KillTimer(2);
				m_is_animating = 0;
				GetDlgItem(IDC_MODIFY_BTN)->Invalidate();
			}
		}
	}

	CDialog::OnTimer(nIDEvent);
}


void JHXpsViewerDlg::OnBnClickedModifyBtn()
{
	m_modify_flag = !m_modify_flag;

	if (m_modify_flag){
		GetDlgItem(IDC_MODIFY_BTN)->SetWindowTextW(L"완 료");
		GetDlgItem(IDC_MODIFY_CANCEL_BTN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COLOR_LIST)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_WIDTH_LIST)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SEND_BUTTON)->ShowWindow(SW_HIDE);
		CClientDC dc(this);
		mp_mem_dc = new CDC();
		mp_mem_dc->CreateCompatibleDC(&dc);
		mp_mem_bitmap = new CBitmap();
		mp_mem_bitmap->CreateCompatibleBitmap(&dc, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight());

		CBitmap *p_old_bitmap = mp_mem_dc->SelectObject(mp_mem_bitmap);
		CRect r(0, 0, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight());
		mp_xps_page_image[m_user_page_index]->Draw(mp_mem_dc->GetSafeHdc(), r);
		mp_mem_dc->SelectObject(p_old_bitmap);

	}
	else {
		GetDlgItem(IDC_MODIFY_BTN)->SetWindowTextW(L"수 정");
		GetDlgItem(IDC_MODIFY_CANCEL_BTN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COLOR_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WIDTH_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SEND_BUTTON)->ShowWindow(SW_SHOW);
		HBITMAP h_old_bmp = mp_xps_page_image[m_user_page_index]->Detach();
		mp_xps_page_image[m_user_page_index]->Attach((HBITMAP)mp_mem_bitmap->Detach());
		//지워진 파일의 이름으로 수정된 이미지가 새롭게 저장
		mp_xps_page_image[m_user_page_index]->Save(m_send_file_name[m_user_page_index], Gdiplus::ImageFormatPNG);
		mp_mem_bitmap->Attach(h_old_bmp);
		m_change_arry[m_user_page_index] = 1;
		RemoveModifyImage();
		

		
	}

	Invalidate();
}

void JHXpsViewerDlg::RemoveModifyImage()
{
	if (mp_mem_dc != NULL){
		mp_mem_dc->DeleteDC();
		delete mp_mem_dc;
		mp_mem_dc = NULL;
	}

	if (mp_mem_bitmap != NULL){
		mp_mem_bitmap->DeleteObject();
		delete mp_mem_bitmap;
		mp_mem_bitmap = NULL;
	}
}

void JHXpsViewerDlg::OnDestroy()
{
	CDialog::OnDestroy();

	RemoveModifyImage();
}

void JHXpsViewerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_is_animating == 1) return;

	if (m_modify_flag == 0){
		CRect r;
		GetClientRect(r);

		if (point.x < 100){
			if (m_xps_page_count - 1 > m_user_page_index) m_user_page_index++;
		}
		else if (point.x > r.right - 100){
			if (m_user_page_index > 0) m_user_page_index--;
		}

		Invalidate();
	}
	else {
		m_is_clicked = 1;
		m_prev_pos = point;
	}

	// CDialogEx::OnLButtonDown(nFlags, point);
}


void JHXpsViewerDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_is_clicked = 0;

	// CDialogEx::OnLButtonUp(nFlags, point);
}


void JHXpsViewerDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_is_clicked == 1){
		//CPen red_pen(PS_SOLID, 2, RGB(255, 0, 0));
		CBitmap *p_old_bitmap = mp_mem_dc->SelectObject(mp_mem_bitmap);
		CPen *p_old_pen = mp_mem_dc->SelectObject(&m_pen);

		mp_mem_dc->MoveTo(m_prev_pos);
		mp_mem_dc->LineTo(point);

		mp_mem_dc->SelectObject(p_old_pen);
		mp_mem_dc->SelectObject(p_old_bitmap);

		//red_pen.DeleteObject();
		m_prev_pos = point;

		Invalidate(FALSE);
	}

	// CDialogEx::OnMouseMove(nFlags, point);
}


void JHXpsViewerDlg::OnBnClickedModifyCancelBtn()
{
	m_modify_flag = 0;

	GetDlgItem(IDC_MODIFY_BTN)->SetWindowTextW(L"수 정");
	GetDlgItem(IDC_MODIFY_CANCEL_BTN)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COLOR_LIST)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_WIDTH_LIST)->ShowWindow(SW_HIDE);
	RemoveModifyImage();

	Invalidate();
}

int JHXpsViewerDlg::GetSendPageCount()
{
	return m_changed_page_count;
}

void JHXpsViewerDlg::OnClickedSendButton()
{
	for (int i = 0; i < m_xps_page_count; i++){
		if (0 == m_change_arry[i]){
			m_file_name_size[i] = 0;
			m_send_file_name[i][0] = NULL;
		}
		else m_changed_page_count++;
	}
	m_send_flag = 1;
	JHXpsViewerDlg::OnOK();
	
}

int JHXpsViewerDlg::GetSendFlag()
{
	return m_send_flag;
}

unsigned short JHXpsViewerDlg::GetSendFileNameSize(int parm_index)
{
	return m_file_name_size[parm_index];
}

void JHXpsViewerDlg::OnSelchangeColorList()//색깔변경시
{
	int color_index = m_color_list.GetCurSel();
	if (color_index != CB_ERR){
		m_pen_color = (COLORREF)m_color_list.GetItemData(color_index);
		m_pen.DeleteObject();//기존의 펜은 제거
		m_pen.CreatePen(PS_SOLID, m_pen_width, m_pen_color);//선택한 색깔로 펜 새로 생성
	}
}


void JHXpsViewerDlg::OnSelchangeWidthList()//굵기변경시
{
	int width_index = m_width_list.GetCurSel();
	if (width_index != CB_ERR){
		m_pen_width = (COLORREF)m_width_list.GetItemData(width_index);
		m_pen.DeleteObject();//기존의 펜은 제거
		m_pen.CreatePen(PS_SOLID, m_pen_width, m_pen_color);//선택한 굵기로 펜 새로 생성
	}
}