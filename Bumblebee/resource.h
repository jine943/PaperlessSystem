//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// JH_BumBleBee.rc에서 사용되고 있습니다.
//
#define IDD_JH_BUMBLEBEE_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_XPS_VIEWER_DLG              129
#define IDB_CLOSE                       130
#define IDB_BITMAP1                     131
#define IDB_LOGO                        131
#define IDB_BITMAP2                     132
#define IDB_                            134
#define IDB_ACCESS                      134
#define IDB_BITMAP_IP                   135
#define IDC_EVENT_LIST                  1000
#define IDC_ID_EDIT                     1001
#define IDC_CONNECT_BTN                 1002
#define IDC_BUTTON1                     1004
#define IDC_CLOSE                       1004
#define IDC_MODIFY_BTN                  1004
#define IDC_STATIC2                     1006
#define IDC_STATIC_ID                   1007
#define IDC_MODIFY_CANCEL_BTN           1008
#define IDC_BUTTON2                     1009
#define IDC_SEND_BUTTON                 1009
#define IDC_COLOR_LIST                  1010
#define IDC_COMBO2                      1011
#define IDC_WIDTH_LIST                  1011
#define IDC_IP_EDIT                     1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
