
// JH_BumBleBeeDlg.h : 헤더 파일
//
#include "JHClientSocket.h"
#include <Ws2tcpip.h>
#pragma once


// CJH_BumBleBeeDlg 대화 상자
class CJH_BumBleBeeDlg : public JHClientSocket
{
private:
	char m_click_flag;
	POINT m_old_pos;
	CFile m_recv_file;
	CString m_recv_file_name;
	int m_recv_file_name_length;
	CBitmapButton m_connect_btn;
	// 0 가로 1 세로 
	int m_screen[2];
	CFile m_send_file;
	wchar_t m_send_file_name[MAX_FILE_COUNT][MAX_PATH];
	unsigned short m_file_name_size[MAX_FILE_COUNT];
	int m_xps_page_count;
	int m_send_index;

	char m_connect_flag;

	////////////
	CString m_server_ip;
	char m_send_file_data[MAX_FILE_DATA_SIZE];
	wchar_t m_send_fixed_file_name[MAX_FILE_COUNT][MAX_PATH];
	unsigned short m_fixed_file_name_size[MAX_FILE_COUNT];
	LoginData m_login_data;
public:
	CJH_BumBleBeeDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	virtual void UserProcessAfterConnect();
	void AddEventString(const wchar_t *ap_string);
	////
	void SetSendFileName();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_JH_BUMBLEBEE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.
	void UserProcessReadData(unsigned char parm_message_id, unsigned int parm_data_size, void *parm_data);
	void EncryptionData(unsigned char *parm_data, unsigned int parm_size);
	void DecryptionData(unsigned char *parm_data, unsigned int parm_size);
	

// 구현입니다.
protected:
	HICON m_hIcon;
	CListBox m_event_list;
	CBrush m_edit_bk_brush;

	// 생성된 메시지 맵 함수
	
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
//	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedConnectBtn();
	afx_msg void OnDestroy();
	CBitmapButton m_close1_btn;
	CBitmapButton m_btn_close;
	afx_msg void OnBnClickedClose();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	
};
