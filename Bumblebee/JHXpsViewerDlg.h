#include "JHClientSocket.h"
#pragma once




// JHXpsViewerDlg ��ȭ �����Դϴ�.

class JHXpsViewerDlg : public CDialog
{
	DECLARE_DYNAMIC(JHXpsViewerDlg)
private:
	CImage *mp_xps_page_image[MAX_FILE_COUNT];
	int m_xps_page_count;

	char m_change_arry[MAX_FILE_COUNT];
	int m_changed_page_count;

	int m_step_count, m_is_animating;
	int m_page_show_count, m_user_page_index;
	int m_send_flag;
	char m_modify_flag, m_is_clicked;

	CDC *mp_mem_dc;
	CBitmap *mp_mem_bitmap;
	CPoint m_prev_pos;
	
	wchar_t m_send_file_name[MAX_FILE_COUNT][MAX_PATH];
	unsigned short m_file_name_size[MAX_FILE_COUNT];

	CPen m_pen;//��
	COLORREF m_pen_color;//�����
	unsigned short int m_pen_width;//�汽��
	CComboBox m_color_list;//����� �޺��ڽ�
	CComboBox m_width_list;//�汽�� �޺��ڽ�

public:
	JHXpsViewerDlg(CWnd* pParent = NULL);   // ǥ�� �������Դϴ�.
	virtual ~JHXpsViewerDlg();
	void SetFileName(wchar_t *parm_p_name, int parm_file_name_size);
	void RemoveModifyImage();
	int GetSendFlag();
	int GetSendPageCount();

	unsigned short GetSendFileNameSize(int parm_index);
	wchar_t(* GetSendFileName())[MAX_PATH];
// ��ȭ ���� �������Դϴ�.
	enum { IDD = IDD_XPS_VIEWER_DLG };

	wchar_t *GetSendFileName(int parm_index)
	{
		return m_send_file_name[parm_index];
	}
	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV �����Դϴ�.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedModifyBtn();
	afx_msg void OnDestroy();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedModifyCancelBtn();
	afx_msg void OnClickedSendButton();
	afx_msg void OnSelchangeColorList();
	afx_msg void OnSelchangeWidthList();
};
