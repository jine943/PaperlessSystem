
// JH_BumBleBeeDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "JH_BumBleBee.h"
#include "JH_BumBleBeeDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CJH_BumBleBeeDlg 대화 상자



CJH_BumBleBeeDlg::CJH_BumBleBeeDlg(CWnd* pParent):JHClientSocket(CJH_BumBleBeeDlg::IDD, pParent)
{
	m_click_flag = 0;
	m_xps_page_count = 0;
	m_send_index = 0;
	m_server_ip = "0";
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_connect_flag = 0;
}

void CJH_BumBleBeeDlg::DoDataExchange(CDataExchange* pDX)
{
	JHClientSocket::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EVENT_LIST, m_event_list);
	DDX_Control(pDX, IDC_CLOSE, m_btn_close);
	DDX_Control(pDX, IDC_CONNECT_BTN, m_connect_btn);
}

BEGIN_MESSAGE_MAP(CJH_BumBleBeeDlg, JHClientSocket)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(IDC_CONNECT_BTN, &CJH_BumBleBeeDlg::OnBnClickedConnectBtn)
ON_WM_DESTROY()
ON_BN_CLICKED(IDC_CLOSE, &CJH_BumBleBeeDlg::OnBnClickedClose)
ON_WM_LBUTTONDOWN()
ON_WM_LBUTTONUP()
ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CJH_BumBleBeeDlg 메시지 처리기

BOOL CJH_BumBleBeeDlg::OnInitDialog()
{
	JHClientSocket::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
	m_btn_close.LoadBitmaps(IDB_CLOSE, NULL, NULL, NULL);
	m_btn_close.SizeToContent();
	m_connect_btn.LoadBitmaps(IDB_ACCESS);
	m_connect_btn.SizeToContent();
	///
	CreateDirectory(L"png", 0);
	//SetSocketData("192.168.0.202", 26000);
	//Connect();
	mp_list_box = &m_event_list;

	m_edit_bk_brush.CreateSolidBrush(RGB(64, 64, 64));
	CFile login_file;
	
	if (TRUE == login_file.Open(L"ClientAccount.dat", CFile::modeRead)){
		int temp_length;

		login_file.Read(&temp_length, sizeof(int));
		DecryptionData((unsigned char *)&temp_length, sizeof(int));

		wchar_t *p_temp = new wchar_t[temp_length];

		login_file.Read(p_temp, temp_length * 2);
		DecryptionData((unsigned char *)p_temp, temp_length * 2);
		CString id(p_temp);

		///id
		login_file.Read(&temp_length, sizeof(int));
		DecryptionData((unsigned char *)&temp_length, sizeof(int));

		p_temp = new wchar_t[temp_length];

		login_file.Read(p_temp, temp_length * 2);
		DecryptionData((unsigned char *)p_temp, temp_length * 2);
		m_server_ip = p_temp;
		
		
		SetDlgItemText(IDC_ID_EDIT, id);
		SetDlgItemText(IDC_IP_EDIT, m_server_ip);
		login_file.Close();
		delete[] p_temp;
	}

	m_screen[0] = GetSystemMetrics(SM_CXSCREEN);
	m_screen[1] = GetSystemMetrics(SM_CYSCREEN);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CJH_BumBleBeeDlg::OnPaint()
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.
	if (IsIconic())
	{

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CRect r;
		GetClientRect(r);
		// 배경색을 변경한다.

		OnCtlColor((CDC *)&dc, this, 1);

		dc.FillSolidRect(r, RGB(255, 235, 51));

		//	JHClientSocket::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CJH_BumBleBeeDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CJH_BumBleBeeDlg::UserProcessReadData(unsigned char parm_message_id, unsigned int parm_data_size, void *parm_data)
{
	if (NM_CHAT_MESSAGE == parm_message_id){
		/////////////////////////////보성/////////////////////////////
		CString str;
		int index = m_event_list.InsertString(-1, (wchar_t *)parm_data);
		m_event_list.SetCurSel(index);
		//////////////////////////////////////////////////////////////

	} else if (NM_SEND_TEST_PAGE == parm_message_id){
		CFile recv_file;
		recv_file.Open(L"a.png", CFile::modeCreate | CFile::modeWrite);
		// modeCreate : 동일한 파일은 덮어쓰기 없으면 생성
		recv_file.Write(parm_data, parm_data_size);
		recv_file.Close();

		CString file_name;
		file_name = L"a.png";
		int file_size = file_name.GetLength() + 1;
		wchar_t *p_file_name = new wchar_t[file_size];
		wcscpy_s(p_file_name, file_size, file_name);

		PostMessage(LM_SEND_FILE_MESSAGE, (WPARAM)p_file_name);

		// ShellExecute(NULL, L"open", L"mspaint.exe", L"\"C:\\tipsware\\TSP1 JinHee\\JHFax\\a.png\"", L"\"C:\\tipsware\\TSP1 JinHee\\JHFax\"", SW_SHOW);

	}
	else if (NM_SEND_FILE_NAME == parm_message_id){
		
		m_recv_file_name = (wchar_t *)parm_data;
		m_recv_file.Open((wchar_t *)parm_data, CFile::modeCreate | CFile::modeWrite);

	}
	else if (NM_SEND_FILE_DATA == parm_message_id){
		m_recv_file.Write(parm_data, parm_data_size);
	}
	else if (NM_LAST_FILE_DATA == parm_message_id){
		m_recv_file.Write(parm_data, parm_data_size);
		m_recv_file.Close();

		wchar_t *p_name = new wchar_t[m_recv_file_name.GetLength() + 1];
		wcscpy_s(p_name, m_recv_file_name.GetLength() + 1, m_recv_file_name);
		m_recv_file_name_length = m_recv_file_name.GetLength() + 1;
		m_xps_page_count++;

		PostMessage(LM_SEND_FILE_MESSAGE, (WPARAM)p_name);
				
	}
	else if (NM_END_SEND_FILE == parm_message_id){
		wchar_t temp_name[36];
		const wchar_t *p_temp_string = (const wchar_t *)m_recv_file_name;
		
		int length = wcslen(p_temp_string + 26) - 4;
		memcpy(temp_name, p_temp_string + 26, length << 1);
		temp_name[length] = 0;

		CString str;
		str.Format(L"가 %d장의 Fax를 전송하였습니다.", m_xps_page_count);
		wcscat_s(temp_name, str);
		m_event_list.InsertString(-1, temp_name);
	}
	else if (NM_LOGIN_FAIL_MESSAGE == parm_message_id){
		MessageBox(L"잘못된 아이디입니다.다시 시도하세요", L"로그인 실패", MB_ICONSTOP);
	}
	else if (parm_message_id == NM_LOGIN_SUCCESS_MESSAGE){
		AddEventString(L"로그인 성공");
		GetDlgItem(IDC_IP_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ID_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CONNECT_BTN)->EnableWindow(FALSE);
		SendFrameData(NM_BBB_SCREEN_DATA, m_screen, sizeof(m_screen));

	}
	else if (NM_CANNOT_START_SEND_FILE == parm_message_id){
		if (0 == parm_data_size){
			AddEventString(CString(L"프라임 비접속 상태 -> 프라임이 접속하면 팩스를 전송하세요"));
		}
	}
	else if (NM_REQUEST_FILE == parm_message_id){
		if (m_send_index < m_xps_page_count){
			m_send_file.Open(m_send_file_name[m_send_index], CFile::modeRead);
		//	AddEventString(CString(L"팩스를 전송합니다 -> ") + m_send_file_name[m_send_index]);
			SendFrameData(NM_SEND_FILE_NAME, m_send_fixed_file_name[m_send_index], m_fixed_file_name_size[m_send_index]);
		}
		else {
			wchar_t temp_name[36];
			const wchar_t *p_temp_string = (const wchar_t *)m_recv_file_name;

			int length = wcslen(p_temp_string + 26) - 4;
			memcpy(temp_name, p_temp_string + 26, length << 1);
			temp_name[length] = 0;

			CString str;
			str.Format(L"에게 %d장의 Fax를 전송하였습니다.", m_xps_page_count);
			wcscat_s(temp_name, str);
			
			m_event_list.InsertString(-1, temp_name);
			AddEventString(CString(L"종이를 아끼셨습니다!"));

			SendFrameData(NM_END_SEND_FILE, NULL, 0);
			m_send_index = 0;
			m_xps_page_count = 0;
			// 팩스 지우기
		}
	}
	else if (NM_NEXT_FILE_DATA == parm_message_id){
		int read_size = m_send_file.Read(m_send_file_data, MAX_FILE_DATA_SIZE);

		if (read_size == MAX_FILE_DATA_SIZE){
			SendFrameData(NM_SEND_FILE_DATA, m_send_file_data, read_size);
		}
		else {
			m_send_file.Close();
			DeleteFile(m_send_file_name[m_send_index]);

	//		AddEventString(CString(L"프라임으로 전송하였습니다. ") + m_send_file_name[m_send_index]);
			m_send_index++;
			SendFrameData(NM_LAST_FILE_DATA, m_send_file_data, read_size);
		}
	}
	
}

void CJH_BumBleBeeDlg::AddEventString(const wchar_t *ap_string)
{
	int index = m_event_list.InsertString(-1, ap_string);
	m_event_list.SetCurSel(index);
}


#include "JHXpsViewerDlg.h"

JHXpsViewerDlg *gp_xps_viewer_dlg;

LRESULT CJH_BumBleBeeDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (LM_SEND_FILE_MESSAGE == message){
		wchar_t *p_file_name = (wchar_t *)wParam;
	
		if (gp_xps_viewer_dlg != NULL){
			gp_xps_viewer_dlg->SetFileName(p_file_name, m_recv_file_name_length);
		}
		else {
			JHXpsViewerDlg ins_dlg(NULL);
			ins_dlg.SetFileName(p_file_name, m_recv_file_name_length);
			gp_xps_viewer_dlg = &ins_dlg;
			ins_dlg.DoModal();
			int changed_page_count = 0;
			if (ins_dlg.GetSendFlag() == 1)
			{
				int total_change_count = ins_dlg.GetSendPageCount(); // 변경된 페이지의 총 수를 반환 3

				// m_xps_page_count ==  5
				for (int i = (m_xps_page_count-1); i >= 0; i--){ // i 4 -> 0
					m_file_name_size[i] = ins_dlg.GetSendFileNameSize(i);
					if (0 != m_file_name_size[i]){
						wcscpy_s(m_send_file_name[total_change_count - changed_page_count - 1], m_file_name_size[i], ins_dlg.GetSendFileName(i));
						changed_page_count++;
						if (changed_page_count == total_change_count) break;
					} // m_send_file_name[0], 1, 2
				}
				// 전송할 페이지 수를 세팅
				m_xps_page_count = total_change_count;
				SetSendFileName();
				SendFrameData(NM_BBB_START_SEND_FILES, NULL, 0);
			}
			gp_xps_viewer_dlg = NULL;
		}
		//m_file_name_size[m_xps_page_count] = m_recv_file_name_length;
		
		
		delete[] p_file_name;
	}
	return JHClientSocket::WindowProc(message, wParam, lParam);
}


void CJH_BumBleBeeDlg::UserProcessAfterConnect()
{
	m_connect_flag = 2;
	SendFrameData(NM_LOGIN_MESSAGE, &m_login_data, sizeof(m_login_data));
}





void CJH_BumBleBeeDlg::OnBnClickedConnectBtn()
{
	//입력한 서버ip
	if (m_connect_flag == 0){
		m_connect_flag = 1;

		int str_ip_length = GetDlgItemText(IDC_IP_EDIT, m_server_ip);
		//입력한 범블비id
		CString str_id;
		int str_id_length = GetDlgItemText(IDC_ID_EDIT, str_id);

		//서버 ip와 범블비id가 입력되었다면
		if (str_ip_length > 0 && str_id_length > 0){
			//서버로 접속
			SetSocketData((CStringA)m_server_ip, 26000);
			Connect();

			//id 체크
			wcscpy_s(m_login_data.id, str_id.GetLength() + 1, str_id);


			CFile login_file;

			if (TRUE == login_file.Open(L"ClientAccount.dat", CFile::modeCreate | CFile::modeWrite)){
				int temp_length = str_id.GetLength() + 1;
				int str_length = temp_length;

				wchar_t *p_temp = new wchar_t[temp_length];
				wcscpy_s(p_temp, temp_length, m_login_data.id);


				EncryptionData((unsigned char *)&temp_length, sizeof(int));
				login_file.Write(&temp_length, sizeof(int));
				EncryptionData((unsigned char *)p_temp, str_length * 2);
				login_file.Write(p_temp, str_length * 2);

				delete[] p_temp; ///아이디를 파일에 쓴다.

				temp_length = m_server_ip.GetLength() + 1;
				str_length = temp_length;
				p_temp = new wchar_t[temp_length];
				wcscpy_s(p_temp, temp_length, m_server_ip);

				EncryptionData((unsigned char *)&temp_length, sizeof(int));
				login_file.Write(&temp_length, sizeof(int));
				EncryptionData((unsigned char *)p_temp, str_length * 2);
				login_file.Write(p_temp, str_length * 2);

				delete[] p_temp;

				login_file.Close();
			}
		}
		else{
			MessageBox(L"IP와 ID를 확인해 주세요.", L"접속 실패", MB_ICONSTOP);
		}
	}
}


void CJH_BumBleBeeDlg::DecryptionData(unsigned char *parm_data, unsigned int parm_size)
{
	for (unsigned int i = 0; i < parm_size; i++){
		*parm_data = (*parm_data) ^ 0x69;
		parm_data++;
	}
}

void CJH_BumBleBeeDlg::EncryptionData(unsigned char *parm_data, unsigned int parm_size)
{
	for (unsigned int i = 0; i < parm_size; i++){
		*parm_data = (*parm_data) ^ 0x69;
		parm_data++;
	}
}


void CJH_BumBleBeeDlg::OnDestroy()
{
	m_edit_bk_brush.DeleteObject();
	for (unsigned short i = 0; i < m_xps_page_count; i++)
	{
		DeleteFile(m_send_file_name[i]);
	}

	JHClientSocket::OnDestroy();
}

HBRUSH CJH_BumBleBeeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_ID)
		{
		

			pDC->SetTextColor(RGB(255, 0, 0));
			pDC->SetBkMode(TRANSPARENT);
			return (HBRUSH)GetStockObject(NULL_BRUSH);;
		}


	
	return (HBRUSH) ::GetStockObject(NULL_BRUSH);
}

void CJH_BumBleBeeDlg::OnBnClickedClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}


BOOL CJH_BumBleBeeDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	HCURSOR hCursor;
	hCursor = AfxGetApp()->LoadStandardCursor(IDC_HAND);

	if (pMsg->message == WM_MOUSEMOVE){
		CButton *p_ok_btn = (CButton *)GetDlgItem(IDC_CLOSE);
		CButton *p_connect_btn = (CButton *)GetDlgItem(IDC_CONNECT_BTN);
		if (p_ok_btn != NULL && p_ok_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
		if (p_connect_btn != NULL && p_connect_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
	}
	return JHClientSocket::PreTranslateMessage(pMsg);
}


void CJH_BumBleBeeDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_click_flag = 1;
	POINT temp_pos;
	GetCursorPos(&temp_pos);
	m_old_pos = temp_pos;
	SetCapture();
	JHClientSocket::OnLButtonDown(nFlags, point);
}


void CJH_BumBleBeeDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_click_flag = 0;
	ReleaseCapture();
	JHClientSocket::OnLButtonUp(nFlags, point);
}


void CJH_BumBleBeeDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (m_click_flag == 1){
		CRect r;
		GetWindowRect(r);
		POINT temp_pos;
		GetCursorPos(&temp_pos);
		MoveWindow(r.left + temp_pos.x - m_old_pos.x, r.top + temp_pos.y - m_old_pos.y, r.Width(), r.Height());
		m_old_pos = temp_pos;
	}
	JHClientSocket::OnMouseMove(nFlags, point);
}


void CJH_BumBleBeeDlg::SetSendFileName()
{
	SYSTEMTIME systime;
	GetLocalTime(&systime);

	CString time_string, file_name;
	time_string.Format(L"%04d%02d%02d_%02d%02d%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
	
	for (int i = (m_xps_page_count - 1); i >= 0; i--){
		file_name.Format(L".\\png\\%s_%03d_%s.png", time_string, i, m_login_data.id);
		m_fixed_file_name_size[i] = 2 * (file_name.GetLength() + 1);
		wcscpy_s(m_send_fixed_file_name[i], m_fixed_file_name_size[i], file_name);
	}
}