// JHXpsViewerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JH_Prime.h"
#include "JHXpsViewerDlg.h"
#include "afxdialogex.h"


// JHXpsViewerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(JHXpsViewerDlg, CDialogEx)

JHXpsViewerDlg::JHXpsViewerDlg(CWnd* pParent) :CDialog(JHXpsViewerDlg::IDD, pParent)
{
	m_xps_page_count = 0;
	m_page_show_count = 0;
	m_is_animating = 1;
	m_user_page_index = 0;

	m_modify_flag = 0;
	m_is_clicked = 0;
	mp_mem_dc = NULL;
	mp_mem_bitmap = NULL;
	m_send_flag = 0;
}

JHXpsViewerDlg::~JHXpsViewerDlg()
{
}

void JHXpsViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(JHXpsViewerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_MODIFY_BTN, &JHXpsViewerDlg::OnBnClickedModifyBtn)
	ON_WM_DESTROY()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MODIFY_CANCEL_BTN, &JHXpsViewerDlg::OnBnClickedModifyCancelBtn)
	ON_BN_CLICKED(IDC_SEND_BUTTON, &JHXpsViewerDlg::OnClickedSendButton)
END_MESSAGE_MAP()


// JHXpsViewerDlg 메시지 처리기입니다.

void JHXpsViewerDlg::SetFileName(wchar_t *parm_p_name, int parm_file_name_size)
{
	m_file_name_size[m_xps_page_count] = parm_file_name_size;
	wcscpy_s(m_send_file_name[m_xps_page_count], m_file_name_size[m_xps_page_count], parm_p_name);

	mp_xps_page_image[m_xps_page_count] = new CImage();
	mp_xps_page_image[m_xps_page_count]->Load(parm_p_name);

	m_xps_page_count++;
	m_user_page_index = m_xps_page_count - 1;
}

void JHXpsViewerDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if (m_modify_flag == 0){
		CRect r(0, 0, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight());
		mp_xps_page_image[m_user_page_index]->Draw(dc.GetSafeHdc(), r);
	}
	else {
		CBitmap *p_old_bitmap = mp_mem_dc->SelectObject(mp_mem_bitmap);
		dc.BitBlt(0, 0, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight(),
			mp_mem_dc, 0, 0, SRCCOPY);
		mp_mem_dc->SelectObject(p_old_bitmap);
	}
}


BOOL JHXpsViewerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	MoveWindow(CRect(mp_xps_page_image[0]->GetWidth()*(-1), 0, 0, mp_xps_page_image[0]->GetHeight()));

	SetTimer(1, 5, NULL);
	GetDlgItem(IDC_SEND_BUTTON)->ShowWindow(SW_HIDE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void JHXpsViewerDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 1){
		MoveWindow(CRect(mp_xps_page_image[0]->GetWidth()*(-1) + m_step_count, 0, m_step_count, mp_xps_page_image[0]->GetHeight()));

		if (m_step_count < mp_xps_page_image[0]->GetWidth()) m_step_count += 20;
		else {
			KillTimer(1);
			m_page_show_count = 1;

			m_step_count = 0;

			if (m_page_show_count < m_xps_page_count){
				SetTimer(2, 5, NULL);
			}
			else {
				m_is_animating = 0;
			}
		}
	}
	else if (nIDEvent == 2){
		CClientDC dc(this);

		CRect r;
		if (m_step_count < mp_xps_page_image[m_page_show_count]->GetWidth()){
			r.SetRect(mp_xps_page_image[m_page_show_count]->GetWidth()*(-1) + m_step_count, 0,
				m_step_count, mp_xps_page_image[m_page_show_count]->GetHeight());
			m_step_count += 20;
			mp_xps_page_image[m_page_show_count]->Draw(dc.GetSafeHdc(), r);
		}
		else {
			r.SetRect(0, 0,
				mp_xps_page_image[m_page_show_count]->GetWidth(), mp_xps_page_image[m_page_show_count]->GetHeight());
			mp_xps_page_image[m_page_show_count]->Draw(dc.GetSafeHdc(), r);

			m_page_show_count++;
			m_step_count = 0;

			if (m_page_show_count < m_xps_page_count){

			} else {
				KillTimer(2);
				m_is_animating = 0;
				GetDlgItem(IDC_MODIFY_BTN)->Invalidate();
			}
		}
	}

	CDialog::OnTimer(nIDEvent);
}


void JHXpsViewerDlg::OnBnClickedModifyBtn()
{
	m_modify_flag = !m_modify_flag;

	if (m_modify_flag){
		GetDlgItem(IDC_MODIFY_BTN)->SetWindowTextW(L"완 료");
		GetDlgItem(IDC_MODIFY_CANCEL_BTN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SEND_BUTTON)->ShowWindow(SW_HIDE);
		CClientDC dc(this);
		mp_mem_dc = new CDC();
		mp_mem_dc->CreateCompatibleDC(&dc);
		mp_mem_bitmap = new CBitmap();
		mp_mem_bitmap->CreateCompatibleBitmap(&dc, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight());

		CBitmap *p_old_bitmap = mp_mem_dc->SelectObject(mp_mem_bitmap);
		CRect r(0, 0, mp_xps_page_image[m_user_page_index]->GetWidth(), mp_xps_page_image[m_user_page_index]->GetHeight());
		mp_xps_page_image[m_user_page_index]->Draw(mp_mem_dc->GetSafeHdc(), r);
		mp_mem_dc->SelectObject(p_old_bitmap);

	}
	else {
		GetDlgItem(IDC_MODIFY_BTN)->SetWindowTextW(L"수 정");
		GetDlgItem(IDC_MODIFY_CANCEL_BTN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SEND_BUTTON)->ShowWindow(SW_SHOW);
		HBITMAP h_old_bmp = mp_xps_page_image[m_user_page_index]->Detach();
		mp_xps_page_image[m_user_page_index]->Attach((HBITMAP)mp_mem_bitmap->Detach());
		//지워진 파일의 이름으로 수정된 이미지가 새롭게 저장
		mp_xps_page_image[m_user_page_index]->Save(m_send_file_name[m_user_page_index], Gdiplus::ImageFormatPNG);
		mp_mem_bitmap->Attach(h_old_bmp);

		RemoveModifyImage();
		

		
	}

	Invalidate();
}

void JHXpsViewerDlg::RemoveModifyImage()
{
	if (mp_mem_dc != NULL){
		mp_mem_dc->DeleteDC();
		delete mp_mem_dc;
		mp_mem_dc = NULL;
	}

	if (mp_mem_bitmap != NULL){
		mp_mem_bitmap->DeleteObject();
		delete mp_mem_bitmap;
		mp_mem_bitmap = NULL;
	}
}

void JHXpsViewerDlg::OnDestroy()
{
	CDialog::OnDestroy();

	RemoveModifyImage();
}

void JHXpsViewerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_is_animating == 1) return;

	if (m_modify_flag == 0){
		CRect r;
		GetClientRect(r);

		if (point.x < 100){
			if (m_xps_page_count - 1 > m_user_page_index) m_user_page_index++;
		}
		else if (point.x > r.right - 100){
			if (m_user_page_index > 0) m_user_page_index--;
		}

		Invalidate();
	}
	else {
		m_is_clicked = 1;
		m_prev_pos = point;
	}

	// CDialogEx::OnLButtonDown(nFlags, point);
}


void JHXpsViewerDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_is_clicked = 0;

	// CDialogEx::OnLButtonUp(nFlags, point);
}


void JHXpsViewerDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_is_clicked == 1){
		CPen red_pen(PS_SOLID, 2, RGB(255, 0, 0));
		CBitmap *p_old_bitmap = mp_mem_dc->SelectObject(mp_mem_bitmap);
		CPen *p_old_pen = mp_mem_dc->SelectObject(&red_pen);

		mp_mem_dc->MoveTo(m_prev_pos);
		mp_mem_dc->LineTo(point);

		mp_mem_dc->SelectObject(p_old_pen);
		mp_mem_dc->SelectObject(p_old_bitmap);

		red_pen.DeleteObject();
		m_prev_pos = point;

		Invalidate(FALSE);
	}

	// CDialogEx::OnMouseMove(nFlags, point);
}


void JHXpsViewerDlg::OnBnClickedModifyCancelBtn()
{
	m_modify_flag = 0;

	GetDlgItem(IDC_MODIFY_BTN)->SetWindowTextW(L"수 정");
	GetDlgItem(IDC_MODIFY_CANCEL_BTN)->ShowWindow(SW_HIDE);
	RemoveModifyImage();

	Invalidate();
}


void JHXpsViewerDlg::OnClickedSendButton()
{
	m_send_flag = 1;
	JHXpsViewerDlg::OnOK();
}
