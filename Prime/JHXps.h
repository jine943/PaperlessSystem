#ifndef _JH_XPS_
#define _JH_XPS_

#include <xpsobjectmodel.h>

//////////////////////////////////////////////////////////////
///////////////////////// VISUAL /////////////////////////////

class JHXpsVisual
{
protected:
	XPS_OBJECT_TYPE m_visual_type;
	
public:
	virtual ~JHXpsVisual(){ }
	XPS_OBJECT_TYPE GetType(){
		return m_visual_type;
	}
	virtual void Draw(CDC *parm_dc){ }
};

//////////////////////////////////////////////////////////////
////////////////////////// GLYPHS ////////////////////////////

class JHXpsGlyphs : public JHXpsVisual
{
private:
	int m_x, m_y;
	CString m_string;
	FLOAT m_font_size;

	CFont m_font;
	CString mp_face_name;

	COLORREF m_text_color;

	XPS_STYLE_SIMULATION m_text_style;

public:
	int SearchStringCount(const wchar_t *parm_p_string);
	JHXpsGlyphs(IXpsOMGlyphs *parm_glyphs);

	void SetGlyphs(int parm_x, int parm_y, const wchar_t *parm_p_string, FLOAT parm_font_size = 11)
	{
		m_x = parm_x;
		m_y = parm_y;
		m_string = parm_p_string;
		m_font_size = parm_font_size;
		mp_face_name = "굴림체";

	}

	void SetPos(int parm_x, int parm_y)
	{
		m_x = parm_x;
		m_y = parm_y;
	}

	void SetString(const wchar_t *parm_p_string)
	{
		m_string = parm_p_string;
	}

	void Draw(CDC *parm_p_dc)
	{
		int weight = FW_NORMAL;
		int font_size = (int)m_font_size;
		bool italic_style = FALSE;
		switch (m_text_style){
			case XPS_STYLE_SIMULATION_BOLD:
				weight = FW_BOLD;
				font_size += 5;
				break;
			case XPS_STYLE_SIMULATION_BOLDITALIC:
				weight = FW_BOLD;
				italic_style = TRUE;
				font_size += 5;
				break;
			case XPS_STYLE_SIMULATION_ITALIC:
				italic_style = TRUE;
				font_size += 5;
				break;
		}

		m_font.CreateFont(font_size, 0, 0, 0, weight, italic_style, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
			CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, mp_face_name);

		CFont *p_old_font = parm_p_dc->SelectObject(&m_font);
		parm_p_dc->SetTextColor(m_text_color);
		parm_p_dc->TextOut(m_x, m_y, m_string);

		parm_p_dc->SelectObject(p_old_font);
		m_font.DeleteObject();
	}
	const wchar_t *GetString()
	{
		return m_string;
	}
};

//////////////////////////////////////////////////////////////
////////////////////////// CANVAS ////////////////////////////


class JHXpsCanvas : public JHXpsVisual
{
protected:
	JHXpsVisual **mp_visual_list;
	UINT32 m_visual_list_count;
public:

	JHXpsCanvas(IXpsOMCanvas *parm_canvas);
	virtual ~JHXpsCanvas();
	virtual void Draw(CDC *parm_dc);
};


//////////////////////////////////////////////////////////////
//////////////////// /////IMAGE_BRUSH ////////////////////////

class JHXpsImageBrush
{
private:
	CImage m_image;
	CRect m_rect;

public:
	JHXpsImageBrush(IXpsOMBrush *parm_brush);
	~JHXpsImageBrush();


	void setviewport(XPS_RECT *parm_view_port){
		m_rect.left = (LONG)parm_view_port->x;
		m_rect.top = (LONG)parm_view_port->y;
		m_rect.right = (LONG)parm_view_port->width;
		m_rect.bottom = (LONG)parm_view_port->height;
	}

	void SetRect(int parm_x, int parm_y){
		m_rect.left += parm_x;
		m_rect.top += parm_y;
		if (!m_image.IsNull()){ 
			m_rect.right = parm_x + m_image.GetWidth();
			m_rect.bottom = parm_y + m_image.GetHeight();
		}
	}

	void DrawImage(CDC *parm_dc)
	{
		m_image.Draw(parm_dc->m_hDC, m_rect);
	}

};


class MyBrush
{
protected:
	XPS_OBJECT_TYPE m_brush_type;

public:
	XPS_OBJECT_TYPE GetBrushType()
	{
		return m_brush_type;
	}
	virtual HRESULT CreateBrush(IXpsOMBrush *parm_brush) { return S_OK; }
	virtual void SetRect(int parm_x, int parm_y) {}
	virtual void DrawImage(CDC *parm_dc) {}
	virtual COLORREF GetColor() { return RGB(255, 255, 255); }
};

class MySolidColorBrush : public MyBrush
{
private:
	COLORREF m_brush_color;

public:
	MySolidColorBrush();
	MySolidColorBrush(IXpsOMBrush *parm_brush);

	virtual HRESULT CreateBrush(IXpsOMBrush *parm_image_brush);
	virtual COLORREF GetColor();
};

class MyImageBrush : public MyBrush
{
private:
	CImage m_image;
	XPS_RECT m_viewport;
	CRect m_rect;

public:
	MyImageBrush();
	MyImageBrush(IXpsOMBrush *parm_brush);

	virtual HRESULT CreateBrush(IXpsOMBrush *parm_brush);
	virtual void SetRect(int parm_x, int parm_y);
	virtual void DrawImage(CDC *parm_dc);
};

class MyFigure
{
private:
	POINT *mp_pos_list;
	int m_pos_count;

	char m_is_filled;
	char m_is_closed;

	int m_type_count;
	char *mp_type_array;

public:
	MyFigure();
	MyFigure(IXpsOMGeometryFigure *parm_figure);
	~MyFigure();

	char GetIsFilled();
	char GetIsClosed();

	POINT *GetPosList();
	int GetPosCount();

	int GetTypeCount();
	char *GetTypeData();

	void SetTypeData(int a_type_count, char *ap_type_array);

	HRESULT CreateFigure(IXpsOMGeometryFigure *parm_figure);
	void BuildPosObject(POINT *parm_pos_list, int parm_pos_count);
};


class MyPath : public JHXpsVisual
{
private:
	MyBrush *mp_brush;

	MyFigure **mp_figure_list;
	int m_figure_count;

public:
	MyPath();
	MyPath(IXpsOMVisual *parm_visual);
	virtual ~MyPath();

	virtual HRESULT Create(IXpsOMVisual *parm_visual);
	HRESULT GetMyGeometry(IXpsOMGeometry * parm_geometry);
	virtual void Draw(CDC *parm_dc);
};


//////////////////////////////////////////////////////////////
////////// PAGE, DOCUMENT, XPS_MANAGER //////////////////////

class JHXpsPage
{
private:
	JHXpsVisual **mp_visual_list;
	UINT32 m_visual_list_count;
	FLOAT m_width, m_height;

public:
	JHXpsPage(IXpsOMPageReference *parm_page_ref);
	~JHXpsPage();
	void DrawPage(CDC *parm_dc);
	float GetWidth();
	float GetHeight();
};


class JHXpsDocument
{
private:
	JHXpsPage **mp_page_list;
	UINT32 m_total_page_count;

public:
	JHXpsDocument(IXpsOMDocument *parm_document);
	~JHXpsDocument();
	JHXpsPage *GetPage(unsigned int parm_page_index);
	void DrawPage(unsigned int parm_index, CDC *parm_dc);
	unsigned int GetTotalPage();
	// HRESULT GetPage(UINT32 parm_index);
	
};


class JHXpsManager
{
private:
	// 인터페이스 객체
	IXpsOMObjectFactory *mp_factory;
	// 패키지란 여러가지 형태의 결과물을 갖고 있는 것
	IXpsOMPackage *mp_package;

	JHXpsDocument **mp_document_list;
	UINT32 m_doc_count;

public:
	JHXpsManager(wchar_t *parm_xps_file_name);
	~JHXpsManager();
	JHXpsDocument *GetDocument(UINT32 parm_index);
	unsigned int GetDocCount();
};

#endif