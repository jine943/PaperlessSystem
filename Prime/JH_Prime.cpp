
// JH_Prime.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "JH_Prime.h"
#include "JH_PrimeDlg.h"
#include "JHLoginDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CJH_PrimeApp

BEGIN_MESSAGE_MAP(CJH_PrimeApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CJH_PrimeApp 생성

CJH_PrimeApp::CJH_PrimeApp()
{
	// 다시 시작 관리자 지원
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CJH_PrimeApp 개체입니다.
CJH_PrimeApp theApp;


// CJH_PrimeApp 초기화
BOOL CJH_PrimeApp::InitInstance()
{
	JHLoginDlg log_dlg;

	if (IDOK == log_dlg.DoModal()){
		CJH_PrimeDlg dlg;
		dlg.SetSocketData((CStringA)log_dlg.GetIP(), 26000);
		dlg.SetIDandPW(log_dlg.GetID(), log_dlg.GetPW());
		m_pMainWnd = &dlg;
		INT_PTR nResponse = dlg.DoModal();
	}

	return FALSE;
}

