#include "JHClientSocket.h"
#pragma once


#define MAX_FILE_COUNT		100

// JHXpsViewerDlg 대화 상자입니다.

class JHXpsViewerDlg : public CDialog
{
	DECLARE_DYNAMIC(JHXpsViewerDlg)
private:
	CImage *mp_xps_page_image[MAX_FILE_COUNT];
	int m_xps_page_count;

	int m_step_count, m_is_animating;
	int m_page_show_count, m_user_page_index;

	char m_modify_flag, m_is_clicked;

	CDC *mp_mem_dc;
	CBitmap *mp_mem_bitmap;
	CPoint m_prev_pos;

	char m_send_flag;
	
	wchar_t m_send_file_name[MAX_FILE_COUNT][MAX_PATH];
	unsigned short m_file_name_size[MAX_FILE_COUNT];


public:
	JHXpsViewerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~JHXpsViewerDlg();
	void SetFileName(wchar_t *parm_p_name, int parm_file_name_size);
	void RemoveModifyImage();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_XPS_VIEWER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedModifyBtn();
	afx_msg void OnDestroy();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedModifyCancelBtn();
	afx_msg void OnClickedSendButton();
};
