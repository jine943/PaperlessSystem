
// JH_PrimeDlg.h : 헤더 파일
//
#include "JHClientSocket.h"
#include "JHXps.h"
#pragma once



#define MAX_FILE_COUNT		100

// CJH_PrimeDlg 대화 상자
class CJH_PrimeDlg : public JHClientSocket
{
private:
	LoginData m_login_data;
	// CString m_id, m_pw;
	unsigned short m_id_len, m_pw_len;
	CBitmapButton m_btn_cancel;
	CBitmapButton m_btn_send;
	CBitmapButton m_btn_set;
	CDC *mp_mem_dc;

	unsigned short m_page_count;
	char m_connect_flag;
	// \\png\\ 시간 _%페이지 _% 아이디.png"
	wchar_t m_send_file_name[MAX_FILE_COUNT][MAX_PATH];
	unsigned short m_file_name_size[MAX_FILE_COUNT];

	int m_send_file_size[MAX_FILE_COUNT];
	int m_send_index;

	CFile m_send_file;
	char m_send_file_data[MAX_FILE_DATA_SIZE];

	// 해상도
	int m_screen[2];

	
	wchar_t m_xps_file_path[256];
	wchar_t m_xps_file_name[256];

	/////////////////////  bbb로부터 받기
	CFile m_recv_file;
	CString m_recv_file_name;
	int m_recv_file_name_length;
	char m_recv_page_count;


public:
	CJH_PrimeDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	void UserProcessReadData(unsigned char parm_message_id, unsigned int parm_data_size, void *parm_data);
	void RequireLogin();
	void AddEventString(const wchar_t *ap_string);

	void MakeXpsToPng(JHXpsManager *parm_xps_manager);

	virtual void UserProcessAfterConnect();
	virtual void UserProcessAfterDissConnect();

	void SetIDandPW(const wchar_t *parm_id, const wchar_t *parm_pw);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_JH_PRIME_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	CListBox m_event_list;
	CProgressCtrl m_send_progress;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedSendBtn();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
protected:
	afx_msg LRESULT OnLmLoginMessage(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedSetBbbBtn();
protected:
	afx_msg LRESULT OnLmBbbList(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedButton2();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	char m_click_flag;
	POINT m_old_pos;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	afx_msg void OnNMCustomdrawSendProgress(NMHDR *pNMHDR, LRESULT *pResult);
};


extern CJH_PrimeDlg *gp_main_dlg;