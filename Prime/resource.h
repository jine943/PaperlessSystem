//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// JH_Prime.rc에서 사용되고 있습니다.
//
#define IDD_JH_PRIME_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_LOGIN_DLG                   129
#define IDD_DIALOG1                     130
#define IDB_BITMAP1                     131
#define IDB_PRIME_LOGO                  131
#define IDD_XPS_VIEWER_DLG              131
#define IDB_BITMAP2                     132
#define IDB_CLOSE                       132
#define IDB_BITMAP3                     133
#define IDB_LOGIN                       133
#define IDB_PRIME                       134
#define IDB_PRIME_HANGLE                134
#define IDB_BITMAP4                     136
#define IDB_SEND                        136
#define IDB_BITMAP5                     137
#define IDB_SET                         137
#define IDB_BITMAP6                     138
#define IDB_NO_SELECT                   138
#define IDB_BITMAP7                     139
#define IDB_WARNING                     139
#define IDB_IP                          141
#define IDB_ID                          143
#define IDB_BITMAP9                     144
#define IDB_PW                          144
#define IDC_EVENT_LIST                  1000
#define IDC_SEND_BTN                    1001
#define IDC_LOGIN_BTN                   1002
#define IDC_ID_EDIT                     1003
#define IDC_PW_EDIT                     1004
#define IDC_BUTTON1                     1004
#define IDC_SET_BBB_BTN                 1004
#define IDC_MODIFY_BTN                  1004
#define IDC_FAVORIT_BBB_EDIT            1005
#define IDC_BUMBLEBEE_LIST              1006
#define IDC_NON_SELECT_BTN              1007
#define IDC_PROGRESS1                   1008
#define IDC_SEND_PROGRESS               1008
#define IDC_MODIFY_CANCEL_BTN           1008
#define IDC_BUTTON2                     1009
#define IDC_SEND_BUTTON                 1009
#define IDC_LOGO                        1010
#define IDC_CANCAL                      1011
#define IDC_WARING                      1012
#define IDC_STATIC1                     1013
#define IDC_IP_EDIT                     1014
#define IDC_STATIC_ID                   1015
#define IDC_STATIC_IP                   1016
#define IDC_STATIC_PW                   1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        145
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
