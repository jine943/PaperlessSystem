
// JH_PrimeDlg.cpp : 구현 팩스
//

#include "stdafx.h"
#include "JH_Prime.h"
#include "JH_PrimeDlg.h"
#include "afxdialogex.h"
#include "JHLoginDlg.h"
#include "BumBleBeeListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
extern CJH_PrimeDlg *gp_main_dlg;
extern BumBleBeeListDlg *gp_bbb_dlg;

#include "JHXpsViewerDlg.h"

JHXpsViewerDlg *gp_xps_viewer_dlg;


CJH_PrimeDlg::CJH_PrimeDlg(CWnd* pParent) :JHClientSocket(CJH_PrimeDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_page_count = 0;
	mp_mem_dc = NULL;
	m_click_flag = 0;
	m_screen[0] = 1920;
	m_screen[1] = 1080;
	m_connect_flag = 0;
	m_recv_page_count = 0;
}

void CJH_PrimeDlg::DoDataExchange(CDataExchange* pDX)
{
	JHClientSocket::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EVENT_LIST, m_event_list);
	DDX_Control(pDX, IDC_SEND_PROGRESS, m_send_progress);
	DDX_Control(pDX, IDC_BUTTON2, m_btn_cancel);
	DDX_Control(pDX, IDC_SEND_BTN, m_btn_send);
	DDX_Control(pDX, IDC_SET_BBB_BTN, m_btn_set);
}

BEGIN_MESSAGE_MAP(CJH_PrimeDlg, JHClientSocket)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DROPFILES()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SEND_BTN, &CJH_PrimeDlg::OnBnClickedSendBtn)
	ON_WM_TIMER()
	ON_MESSAGE(LM_LOGIN_MESSAGE, &CJH_PrimeDlg::OnLmLoginMessage)
	ON_BN_CLICKED(IDC_SET_BBB_BTN, &CJH_PrimeDlg::OnBnClickedSetBbbBtn)
	ON_MESSAGE(LM_BBB_LIST, &CJH_PrimeDlg::OnLmBbbList)
	ON_BN_CLICKED(IDC_BUTTON2, &CJH_PrimeDlg::OnBnClickedButton2)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CTLCOLOR()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SEND_PROGRESS, &CJH_PrimeDlg::OnNMCustomdrawSendProgress)
END_MESSAGE_MAP()


// CJH_PrimeDlg 메시지 처리기
// OnInitDlg 에서 다이얼로그 생성하지 말자 
// 메시지 처리 루틴이 꼬인다
// OnClose();와 EndDialog(IDOK); 를 사용하고
// 사용하지 말자 PostMessage(WM_DESTROY, 0, 0); 
BOOL CJH_PrimeDlg::OnInitDialog()
{
	JHClientSocket::OnInitDialog();

	CreateDirectory(L"png", NULL);

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
	///////////////// 그림 
	m_btn_cancel.LoadBitmaps(IDB_CLOSE, NULL, NULL, NULL);
	m_btn_cancel.SizeToContent();
	m_btn_send.LoadBitmaps(IDB_SEND, NULL, NULL, NULL);
	m_btn_send.SizeToContent();
	m_btn_set.LoadBitmaps(IDB_SET, NULL, NULL, NULL);
	m_btn_set.SizeToContent();
	// 192.168.123.165
	// 172.30.1.5
	//"192.158.0.10"
	Connect();

	mp_list_box = &m_event_list;

	mp_mem_dc = new CDC;
	CClientDC dc(this);
	mp_mem_dc->CreateCompatibleDC(&dc);

	
	ShowWindow(SW_HIDE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CJH_PrimeDlg::OnPaint()
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

	if (IsIconic())
	{
	
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CRect r;
		GetClientRect(r);
		// 배경색을 변경한다.
		dc.FillSolidRect(r, RGB(153, 255, 51));
		JHClientSocket::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CJH_PrimeDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CJH_PrimeDlg::UserProcessAfterConnect()
{
	RequireLogin();
}

void CJH_PrimeDlg::UserProcessAfterDissConnect()
{
	MessageBox(L"서버 접속 실패로 로그인하지 못했습니다..", L"프로그램을 종료합니다.", MB_ICONSTOP);
	EndDialog(IDOK);
}

void CJH_PrimeDlg::OnDropFiles(HDROP hDropInfo)
{
	CString str;
	GetDlgItemText(IDC_FAVORIT_BBB_EDIT, str);

	if (/*!str.IsEmpty() && */ NULL != hDropInfo){
		int count = DragQueryFile(hDropInfo, 0xFFFFFFFF, NULL, 0), file_name_size = 0;
		wchar_t *p_name = NULL;

		if (1 == count) {
			file_name_size = DragQueryFile(hDropInfo, 0, NULL, 0);

			p_name = new wchar_t[file_name_size + 1];

			DragQueryFile(hDropInfo, 0, p_name, file_name_size + 1);

			if (NULL != wcsstr(p_name, L".xps")){
				 // AddEventString(p_name);

				

				JHXpsManager *p_xps_manager = new JHXpsManager(p_name); // _T(".\\xps\\11st.xps")
				MakeXpsToPng(p_xps_manager);
				delete p_xps_manager;

				

			}
			else AddEventString(L"XPS 문서가 아닙니다");

			delete[] p_name;
		}
	} else AddEventString(L"팩스를 전송할 범블비를 선택하세요");

	JHClientSocket::OnDropFiles(hDropInfo);
}


void CJH_PrimeDlg::MakeXpsToPng(JHXpsManager *parm_xps_manager)
{
	CClientDC dc(this);
	

	//CString str;
	//str.Format(L"%f %f", m_width, m_height);
	//MessageBox(L"XPS RECT", str, NULL);

	
	JHXpsDocument *p_xps_document = parm_xps_manager->GetDocument(0);
	unsigned int total_page_count = p_xps_document->GetTotalPage();
	JHXpsPage *p_page = p_xps_document->GetPage(0);
	//p_page->GetWidth(), p_page->GetHeight()
	CRect bmp_rect(0, 0, m_screen[0], m_screen[1]);
	CBitmap mem_bmp, *p_old_bitmap;
	mem_bmp.CreateCompatibleBitmap(&dc, bmp_rect.Width(), bmp_rect.Height()); // 임시 크기 비트맵 


	CImage png_image;

	SYSTEMTIME systime;
	GetLocalTime(&systime);

	CString time_string, file_name;
	time_string.Format(L"%04d%02d%02d_%02d%02d%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
	m_page_count = total_page_count;
	int old_bk_color = mp_mem_dc->SetBkMode(TRANSPARENT);
	CPen *p_old_pen = (CPen *)mp_mem_dc->SelectStockObject(WHITE_PEN);
	p_old_bitmap = mp_mem_dc->SelectObject(&mem_bmp);

	for (unsigned int i = 0; i < total_page_count; i++){
		file_name.Format(L".\\png\\%s_%03d_%s.png", time_string, i, m_login_data.id);

		mp_mem_dc->FillSolidRect(bmp_rect, RGB(255, 255, 255));

		p_xps_document->DrawPage(i, mp_mem_dc);
	
		// GetLength 는 멀티바이트로 계산해 나온다
		m_file_name_size[i] = (file_name.GetLength() + 1) * 2;
		wcscpy_s(m_send_file_name[i], m_file_name_size[i], file_name);

		// AddEventString(m_send_file_name[i]);
		png_image.Attach((HBITMAP)mem_bmp.GetSafeHandle());
		png_image.Save(file_name, Gdiplus::ImageFormatPNG);
		png_image.Detach();
	}	

	// AddEventString(L"변환이 완료되었습니다.");

	PostMessage(WM_COMMAND, IDC_SEND_BTN);
	mp_mem_dc->SelectObject(p_old_pen);
	mp_mem_dc->SelectObject(p_old_bitmap);
	mp_mem_dc->SetBkMode(old_bk_color);
	mem_bmp.DeleteObject();
	// delete p_xps_document;
}

void CJH_PrimeDlg::AddEventString(const wchar_t *ap_string)
{
	int index = m_event_list.InsertString(-1, ap_string);
	m_event_list.SetCurSel(index);
}

void CJH_PrimeDlg::UserProcessReadData(unsigned char parm_message_id, unsigned int parm_data_size, void *parm_data)
{
	if (NM_CHAT_MESSAGE == parm_message_id){
		AddEventString((wchar_t *)parm_data);
	}
	else if (NM_LOGIN_SUCCESS_MESSAGE == parm_message_id){
		wchar_t *p_stream = new wchar_t[parm_data_size / 2];
		memcpy(p_stream, parm_data, parm_data_size);
		PostMessage(LM_LOGIN_MESSAGE, parm_data_size, (LPARAM)p_stream);
		m_connect_flag = 2;
	}
	else if (NM_LOGIN_FAIL_MESSAGE == parm_message_id){
		PostMessage(LM_LOGIN_MESSAGE, 0, 0);
	}
	else if (NM_SEND_BBB_LIST == parm_message_id){
		BBB_State *p_stream = new BBB_State[parm_data_size / sizeof(BBB_State)];
		memcpy(p_stream, parm_data, parm_data_size);
		PostMessage(LM_BBB_LIST, parm_data_size, (LPARAM)p_stream);
	}
	else if (NM_REQUEST_FILE == parm_message_id){
		if (m_send_index >= 0){
			m_send_file.Open(m_send_file_name[m_send_index], CFile::modeRead);
			//CString str;
			//str.Format(L"%d 번째 팩스를 전송합니다! ", m_page_count - m_send_index);
			//AddEventString(str); // + m_send_file_name[m_send_index]
			SendFrameData(NM_SEND_FILE_NAME, m_send_file_name[m_send_index], m_file_name_size[m_send_index]);
		}
		else {

			wchar_t temp_name[36];
			const wchar_t *p_temp_string = (const wchar_t *)m_send_file_name;

			int length = wcslen(p_temp_string + 26) - 4;
			memcpy(temp_name, p_temp_string + 26, length << 1);
			temp_name[length] = 0;

			CString favorit_bbb;
			GetDlgItemText(IDC_FAVORIT_BBB_EDIT, favorit_bbb);

			CString str;
			str.Format(L"에게 %d장의 Fax를 전송하였습니다.", m_page_count);

			AddEventString(favorit_bbb + str);
			AddEventString(L"종이를 아끼셨습니다!");

			SendFrameData(NM_END_SEND_FILE, NULL, 0);
			// GetDlgItem(IDC_SEND_BUTTON)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_SEND_PROGRESS)->ShowWindow(SW_HIDE);
		}
	}
	else if (NM_NEXT_FILE_DATA == parm_message_id){
		int read_size = m_send_file.Read(m_send_file_data, MAX_FILE_DATA_SIZE);

		if (read_size == MAX_FILE_DATA_SIZE){
			SendFrameData(NM_SEND_FILE_DATA, m_send_file_data, read_size);
		}
		else {
			m_send_file.Close();
			DeleteFile(m_send_file_name[m_send_index]);

			//CString str;
			//str.Format(L"%d 번째 팩스 전송을 완료했습니다 ♣ ", m_page_count - m_send_index);
			//AddEventString(str); // + m_send_file_name[m_send_index]
			//AddEventString(CString(L"팩스 전송을 완료했습니다 종이를 아끼셨습니다! -> ") + m_send_file_name[m_send_index]);
			m_send_index--;
			SendFrameData(NM_LAST_FILE_DATA, m_send_file_data, read_size);
		}
		m_send_progress.SetPos(m_send_progress.GetPos() + read_size);
	}
	else if (NM_FAVORIT_BBB_DATA == parm_message_id){
		char *p_data = (char *)parm_data;
		char is_login = *p_data;
		if (is_login == 1){
			
			memcpy(m_screen, p_data + 1, 8);
			SetTimer(1, 1000, NULL);
		}
		else {
			KillTimer(1);
		}
	}
	else if (NM_CANNOT_START_SEND_FILE == parm_message_id){
		if (0 == parm_data_size){
			CString favorit_bbb;
			GetDlgItemText(IDC_FAVORIT_BBB_EDIT, favorit_bbb);
			AddEventString(favorit_bbb + CString(L"범블비 비접속 상태 -> 범블비가 접속하면 팩스를 전송하세요"));
			GetDlgItem(IDC_SEND_PROGRESS)->ShowWindow(SW_HIDE);
		}
	}
	else if (NM_SEND_FILE_NAME == parm_message_id){
		m_recv_file_name = (wchar_t *)parm_data;
		m_recv_file.Open((wchar_t *)parm_data, CFile::modeCreate | CFile::modeWrite);
	}
	else if (NM_SEND_FILE_DATA == parm_message_id){
		m_recv_file.Write(parm_data, parm_data_size);
	}
	else if (NM_LAST_FILE_DATA == parm_message_id){
		m_recv_file.Write(parm_data, parm_data_size);
		m_recv_file.Close();

		wchar_t *p_name = new wchar_t[m_recv_file_name.GetLength() + 1];
		wcscpy_s(p_name, m_recv_file_name.GetLength() + 1, m_recv_file_name);
		m_recv_file_name_length = m_recv_file_name.GetLength() + 1;
		PostMessage(LM_SEND_FILE_MESSAGE, (WPARAM)p_name);
		m_recv_page_count++;
	}
	else if (NM_END_SEND_FILE == parm_message_id){
		wchar_t temp_name[36];
		const wchar_t *p_temp_string = (const wchar_t *)m_recv_file_name;

		int length = wcslen(p_temp_string + 26) - 4;
		memcpy(temp_name, p_temp_string + 26, length << 1);
		temp_name[length] = 0;

		CString str;
		str.Format(L"가 %d장의 Fax를 전송하였습니다.", m_recv_page_count);
		wcscat_s(temp_name, str);
		m_event_list.InsertString(-1, temp_name);
		m_recv_page_count = 0;
	}

}

void CJH_PrimeDlg::OnDestroy()
{
	JHClientSocket::OnDestroy();

	for (unsigned short i = 0; i < m_page_count; i++)
	{
		DeleteFile(m_send_file_name[i]);
	}

	if (NULL != mp_mem_dc){
		mp_mem_dc->DeleteDC();
		delete mp_mem_dc;
	}
}


void CJH_PrimeDlg::OnBnClickedSendBtn()
{
	int total_size = 0;
	CFileStatus temp_file;
	if (m_page_count > 0){
		for (int i = 0; i < m_page_count; i++){
			CFile::GetStatus(m_send_file_name[i], temp_file);

			m_send_file_size[i] = (int)temp_file.m_size;
			total_size += (int)temp_file.m_size;
		}
		
		GetDlgItem(IDC_SEND_PROGRESS)->ShowWindow(SW_SHOW);
		m_send_progress.SetRange(0, total_size);

		m_send_index = m_page_count - 1;
		SendFrameData(NM_START_SEND_FILES, NULL, 0);
	}
	else AddEventString(L"전송할 팩스가 없습니다.");
}


void CJH_PrimeDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 1){
		HWND h_wnd = ::FindWindowEx(NULL, NULL, L"#32770", L"다음 이름으로 프린터 출력 저장");
		if (h_wnd != NULL){
			wchar_t temp[256], temp_name[128];
			RECT r;
			CString str;

			int max_value = 0, temp_value;
			HWND h_ui_wnd = ::FindWindowEx(h_wnd, NULL, NULL, NULL), h_target_wnd = NULL;
			while (h_ui_wnd != NULL){
				GetClassName(h_ui_wnd, temp_name, 128);
				if (wcscmp(temp_name, L"LISTBOX")){
					::GetWindowRect(h_ui_wnd, &r);
					temp_value = (r.right - r.left)*(r.bottom - r.top);
					if (temp_value > max_value){
						max_value = temp_value;
						h_target_wnd = h_ui_wnd;
					}
				}

				h_ui_wnd = ::FindWindowEx(h_wnd, h_ui_wnd, NULL, NULL);
			}

			if (h_target_wnd != NULL){
				HWND h_ui_wnd = ::FindWindowEx(h_target_wnd, NULL, L"DirectUIHWND", NULL);

				if (h_ui_wnd != NULL){
					h_target_wnd = h_ui_wnd;

					HWND h_ui_wnd = ::FindWindowEx(h_target_wnd, NULL, L"FloatNotifySink", NULL);
					while (h_ui_wnd != NULL){
						::GetWindowRect(h_ui_wnd, &r);
						str.Format(L"%d, %d, %d, %d -> %d, %d", r.left, r.top, r.right, r.bottom, r.right - r.left, r.bottom - r.top);
						GetClassName(h_ui_wnd, temp, 256);
						// m_event_list.InsertString(-1, temp);
						// m_event_list.InsertString(-1, str);

						if (h_ui_wnd != NULL){
							HWND h_temp_wnd = ::FindWindowEx(h_ui_wnd, NULL, L"ComboBox", NULL);

							if (h_temp_wnd != NULL){
								GetClassName(h_temp_wnd, temp, 256);
								// m_event_list.InsertString(-1, temp);

								HWND h_edit_wnd = ::FindWindowEx(h_temp_wnd, NULL, L"Edit", NULL);
								if (h_edit_wnd != NULL){
									// m_event_list.InsertString(-1, L"에디트 찾음");
									
									SYSTEMTIME systime;
									GetLocalTime(&systime);
									
									CString xps_file_name;

									xps_file_name.Format(L"%04d%02d%02d_%02d%02d%02d.xps", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
									wcscpy_s(m_xps_file_name, xps_file_name);

									CString temp_file;
									temp_file.Format(L"c:\\temp\\%s", xps_file_name);
									wcscpy_s(m_xps_file_path, temp_file);

									CreateDirectory(L"c:\\temp", NULL);
									
									wcscpy_s(temp, m_xps_file_name);
									::SendMessage(h_edit_wnd, WM_SETTEXT, 256, (LPARAM)m_xps_file_path);
									// ::GetWindowText(h_edit_wnd, temp, 256);
									// ::SetWindowText(h_edit_wnd, L"dkijef");
									// m_event_list.InsertString(-1, temp);
									::PostMessage(h_wnd, WM_COMMAND, IDOK, 0);
									KillTimer(1);
									
									SetTimer(2, 3000, NULL);
										AddEventString(L"잠시만 기다려 주시와요~ XPS 팩스이 생성되고 있습니다.");
										break;
								}
							}
						}
						h_ui_wnd = ::FindWindowEx(h_target_wnd, h_ui_wnd, L"FloatNotifySink", NULL);
					}
				}
			}
		} 
	}
	else if (nIDEvent == 2){
		// 3초 있다가 xps 만들기
		KillTimer(2);
		SetTimer(1, 1000, NULL);

		/*wchar_t temp[256];
		CString temp_file;
		temp_file.Format(L"c:\\temp\\%s", m_xps_file_name);
		wcscpy_s(temp, temp_file);*/
		JHXpsManager *p_xps_manager = new JHXpsManager(m_xps_file_path);
		
		MakeXpsToPng(p_xps_manager);
		delete p_xps_manager;
		DeleteFile(m_xps_file_name);
	}

	JHClientSocket::OnTimer(nIDEvent);
}


void MakeWChartStream(char **parm_p_data, wchar_t *parm_str1, unsigned short parm_str1_len, wchar_t *parm_str2, unsigned short parm_str2_len)
{
	*parm_p_data = new char[4 + parm_str1_len + parm_str2_len];

	char *p_data = *parm_p_data;

	*(unsigned short *)p_data = parm_str1_len;
	p_data += 2;
	wcscpy_s((wchar_t *)p_data, parm_str1_len, parm_str1);
	p_data += parm_str1_len;

	*(unsigned short *)p_data = parm_str2_len;
	p_data += 2;
	wcscpy_s((wchar_t *)p_data, parm_str2_len, parm_str2);
}


void CJH_PrimeDlg::SetIDandPW(const wchar_t *parm_id, const wchar_t *parm_pw)
{
	wcscpy_s(m_login_data.id, 32, parm_id);
	wcscpy_s(m_login_data.pw, 32, parm_pw);
}

void CJH_PrimeDlg::RequireLogin()
{	
	m_connect_flag = 1;
	SendFrameData(NM_LOGIN_MESSAGE, &m_login_data, sizeof(m_login_data));
}

void MakeWChartStreamFromStr(char **parm_p_data, CString parm_str1, unsigned short parm_str1_len, CString parm_str2, unsigned short parm_str2_len)
{
	*parm_p_data = new char[4 + parm_str1_len * 2 + parm_str2_len * 2];

	char *p_pos = *parm_p_data;

	*(unsigned short *)p_pos = parm_str1_len;
	p_pos += 2;
	wcscpy_s((wchar_t *)p_pos, parm_str1_len, parm_str1);
	p_pos += parm_str1_len * 2;

	*(unsigned short *)p_pos = parm_str2_len;
	p_pos += 2;
	wcscpy_s((wchar_t *)p_pos, parm_str2_len, parm_str2);
}




// wparam 로그인 성공 유무 
// lParam favorit bbb 정보 
afx_msg LRESULT CJH_PrimeDlg::OnLmLoginMessage(WPARAM wParam, LPARAM lParam)
{
	if (wParam){
		ShowWindow(SW_SHOW);
		AddEventString(L"로그인 성공");

		BBB_State *p_bbb_sate = (BBB_State *)lParam;
		// favorit bbb 접속시 탐색기 감시
		if (p_bbb_sate->state == 1) SetTimer(1, 1000, NULL);
		memcpy(m_screen, p_bbb_sate->screen, 8);

		SetDlgItemText(IDC_FAVORIT_BBB_EDIT, p_bbb_sate->name);
		delete[] p_bbb_sate; //delete[] (wchar_t *)lParam;
	}
	else {
		MessageBox(L"로그인에 실패하였습니다.", L"프로그램을 종료합니다.", MB_ICONSTOP);
		// m_event_list.InsertString(-1, str);
		delete[](wchar_t *)lParam;
		EndDialog(IDOK);
	}
	return 0;
}

void CJH_PrimeDlg::OnBnClickedSetBbbBtn()
{
	if (mh_socket != INVALID_SOCKET) SendFrameData(NM_REQUEST_BBB_LIST, NULL, 0);
}


afx_msg LRESULT CJH_PrimeDlg::OnLmBbbList(WPARAM wParam, LPARAM lParam)
{
	BumBleBeeListDlg bbb_dlg;

	bbb_dlg.SetBBB((int)wParam / sizeof(BBB_State), (BBB_State *)lParam);
	if (IDOK == bbb_dlg.DoModal()){
		wchar_t *p_data = bbb_dlg.GetSelectedID();
		if (mh_socket != INVALID_SOCKET) SendFrameData(NM_SELECT_BBB_NAME, p_data, (wcslen(p_data) + 1) * 2);
		SetDlgItemText(IDC_FAVORIT_BBB_EDIT, p_data);
	}
	// delete[] lParam;
	return 0;
}


void CJH_PrimeDlg::OnBnClickedButton2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}


BOOL CJH_PrimeDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	HCURSOR hCursor;
	hCursor = AfxGetApp()->LoadStandardCursor(IDC_HAND);

	if (pMsg->message == WM_MOUSEMOVE){
		CButton *p_close_btn = (CButton *)GetDlgItem(IDC_BUTTON2);
		CButton *p_set_btn = (CButton *)GetDlgItem(IDC_SET_BBB_BTN);
		CButton *p_send_btn = (CButton *)GetDlgItem(IDC_SEND_BTN);

		if (p_close_btn != NULL && p_close_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
		if (p_send_btn != NULL && p_send_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);		
		if (p_set_btn != NULL && p_set_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
	}
	
	return JHClientSocket::PreTranslateMessage(pMsg);
}


void CJH_PrimeDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_click_flag = 1;
	POINT temp_pos;
	GetCursorPos(&temp_pos);
	m_old_pos = temp_pos;
	SetCapture();
	JHClientSocket::OnLButtonDown(nFlags, point);
}


void CJH_PrimeDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_click_flag = 0;
	ReleaseCapture();

	JHClientSocket::OnLButtonUp(nFlags, point);
}


void CJH_PrimeDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (m_click_flag == 1){
		CRect r;
		GetWindowRect(r);
		POINT temp_pos;
		GetCursorPos(&temp_pos);
		MoveWindow(r.left + temp_pos.x - m_old_pos.x, r.top + temp_pos.y - m_old_pos.y, r.Width(), r.Height());
		m_old_pos = temp_pos;
	}
	JHClientSocket::OnMouseMove(nFlags, point);
}


HBRUSH CJH_PrimeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = JHClientSocket::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if (pWnd->GetDlgCtrlID() == IDC_STATIC1){
		pDC->SetBkColor(RGB(153, 255, 51));
		CBrush temp_hr = RGB(153, 255, 51);
		hbr = temp_hr;
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


LRESULT CJH_PrimeDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (LM_SEND_FILE_MESSAGE == message){
		wchar_t *p_file_name = (wchar_t *)wParam;

		wchar_t temp_path[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, temp_path);
		CString str(temp_path);
		str = str + p_file_name;
		ShellExecute(NULL, L"open", str, NULL, NULL, SW_SHOW);
		/*

		if (gp_xps_viewer_dlg != NULL){
			gp_xps_viewer_dlg->SetFileName(p_file_name, m_recv_file_name_length);
		}
		else {
			JHXpsViewerDlg ins_dlg(NULL);
			ins_dlg.SetFileName(p_file_name, m_recv_file_name_length);
			gp_xps_viewer_dlg = &ins_dlg;
			ins_dlg.DoModal();
			gp_xps_viewer_dlg = NULL;
		}
		*/
		delete[] p_file_name;

		return 1;
	}
	else{
		return JHClientSocket::WindowProc(message, wParam, lParam);
	}
}


void CJH_PrimeDlg::OnNMCustomdrawSendProgress(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}
