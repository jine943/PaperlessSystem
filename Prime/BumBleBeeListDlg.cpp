// BumBleBeeListDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JH_Prime.h"
#include "BumBleBeeListDlg.h"
#include "afxdialogex.h"

BumBleBeeListDlg *gp_bbb_dlg;
// BumBleBeeListDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(BumBleBeeListDlg, CDialogEx)

BumBleBeeListDlg::BumBleBeeListDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(BumBleBeeListDlg::IDD, pParent)
{
	m_bbb_count = 0;
	mp_bbb_list = NULL;
}

BumBleBeeListDlg::~BumBleBeeListDlg()
{
	delete[] mp_bbb_list;
}

void BumBleBeeListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUMBLEBEE_LIST, m_bbb_list);
	DDX_Control(pDX, IDC_CANCAL, m_cancel_btn);
	DDX_Control(pDX, IDC_NON_SELECT_BTN, m_no_select_btn);
}

BEGIN_MESSAGE_MAP(BumBleBeeListDlg, CDialogEx)
	ON_BN_CLICKED(IDC_NON_SELECT_BTN, &BumBleBeeListDlg::OnBnClickedNonSelectBtn)
	ON_LBN_DBLCLK(IDC_BUMBLEBEE_LIST, &BumBleBeeListDlg::OnDblclkBumblebeeList)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_BN_CLICKED(IDC_CANCAL, &BumBleBeeListDlg::OnBnClickedCancal)
	ON_WM_PAINT()
//	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// BumBleBeeListDlg 메시지 처리기입니다.


void BumBleBeeListDlg::OnBnClickedNonSelectBtn()
{
	m_selected_id[0] = 0;
	CDialogEx::OnOK();
}


void BumBleBeeListDlg::OnDblclkBumblebeeList()
{
	int index = m_bbb_list.GetCurSel();
	if (index != LB_ERR) m_bbb_list.GetText(index, m_selected_id);
	else m_selected_id[0] = 0;
	
	CDialogEx::OnOK();
}


void BumBleBeeListDlg::SetBBB(int parm_count, BBB_State *parm_p_list)
{
	m_bbb_count = parm_count;
	mp_bbb_list = parm_p_list;
}

BOOL BumBleBeeListDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	///////////////
	m_cancel_btn.LoadBitmaps(IDB_CLOSE);
	m_cancel_btn.SizeToContent();
	m_no_select_btn.LoadBitmaps(IDB_NO_SELECT);
	m_no_select_btn.SizeToContent();

	if (m_bbb_count > 0){
		int index;
		for (int i = 0; i < m_bbb_count; i++){
			index = m_bbb_list.InsertString(-1, mp_bbb_list[i].name);
			m_bbb_list.SetItemData(index, mp_bbb_list[i].state);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void BumBleBeeListDlg::OnOK()
{
	OnDblclkBumblebeeList();
}

wchar_t *BumBleBeeListDlg::GetSelectedID()
{
	return m_selected_id;
}

void BumBleBeeListDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDS)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (nIDCtl == IDC_BUMBLEBEE_LIST){
		// 리스트박스에 등록된 항목이 없거나 실제 항목의 범위를 벗어난 지역을 그리는 
		// 경우라면 출력을 무시한다.
		if (lpDS->itemID >= (UINT)m_bbb_list.GetCount() || !m_bbb_list.GetCount()) return;
		CString str;

		// HDC -> CDC 로 변환한다.
		CDC *p_dc = CDC::FromHandle(lpDS->hDC);
		// RECT -> CRect 로 변환한다.
		CRect r(lpDS->rcItem);
		// 문자열을 출력할때 배경색을 무시하도록 설정한다.
		int old_mode = p_dc->SetBkMode(TRANSPARENT);

		// 출력시 항목을 개별적으로 출력하기 때문에 현재 출력할 문자열이 어떤것인지 얻는다.
		m_bbb_list.GetText(lpDS->itemID, str);
		switch (lpDS->itemAction){
		case ODA_DRAWENTIRE: case ODA_FOCUS: case ODA_SELECT:
			if (m_bbb_list.GetItemData(lpDS->itemID)){
				str += "         접속 O";
				p_dc->SetTextColor(RGB(0, 0, 0));
			}
			else{
				p_dc->SetTextColor(RGB(190, 190, 190));
				str += "         접속 X";
			}

			if (lpDS->itemState & ODS_SELECTED){
				// 커서가 설정되어 있는 항목을 출력하는 경우
				// 커서를 나타내기 위해서 커서색상으로 사각형을 그린다.
				p_dc->FillSolidRect(r, RGB(0, 128, 255));

				// 문자열을 출력한다.
				//p_dc->SetTextColor(RGB(0, 0, 0));
				//p_dc->TextOut(r.left + 5, r.top + 2, str);
			}
			else 
			{
				// 커서가 설정되어 있지 않은 항목을 출력하는 경우
				// 커서가 없음을 나타내기 위해서 배경색으로 사각형을 그린다.
				//p_dc->FillSolidRect(r, RGB(255, 255, 255));
				// 문자열을 출력한다.
				p_dc->FillSolidRect(r, RGB(255, 255, 255));
			}
			p_dc->TextOut(r.left + 5, r.top + 2, str);
			break;
		default:
			break;
		}
		// 문자열 출력시 배경색 출력 속성을 본래 속성으로 복구한다.
		p_dc->SetBkMode(old_mode);
	}
	else CDialogEx::OnDrawItem(nIDCtl, lpDS);
}


void BumBleBeeListDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (nIDCtl == IDC_BUMBLEBEE_LIST){
		lpMeasureItemStruct->itemHeight = 20;
		return;
	}
	CDialogEx::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}


void BumBleBeeListDlg::OnBnClickedCancal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}


void BumBleBeeListDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CRect r;
	GetClientRect(r);
	// 배경색을 변경한다.
	dc.FillSolidRect(r, RGB(153, 255, 51));
	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}


BOOL BumBleBeeListDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	HCURSOR hCursor;
	hCursor = AfxGetApp()->LoadStandardCursor(IDC_HAND);

	if (pMsg->message == WM_MOUSEMOVE){
		CButton *p_close_btn = (CButton *)GetDlgItem(IDC_CANCAL);
		CButton *p_no_select_btn = (CButton *)GetDlgItem(IDC_NON_SELECT_BTN);

		if (p_close_btn != NULL && p_close_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
		if (p_no_select_btn != NULL && p_no_select_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
		
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


//HBRUSH BumBleBeeListDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
//{
//	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
//
//	// TODO:  여기서 DC의 특성을 변경합니다.
//	
//	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
//	return hbr;
//}
