#include "stdafx.h"
#include "JHClientSocket.h"


#define LM_SOCKET_MESSAGE	20000
#define KEY_VALUE			16

IMPLEMENT_DYNAMIC(JHClientSocket, CDialog)

JHClientSocket::JHClientSocket(UINT nIDTemplate, CWnd* pParentWnd) : CDialog(nIDTemplate, pParentWnd)
{
	WSADATA data;
	// 소켓 버전, 가져온 정보 저장하는 구조체 변수 
	WSAStartup(0x0202, &data);
	mh_socket = INVALID_SOCKET;
	mp_list_box = NULL;
	m_socket_flag = 0;
}


void JHClientSocket::SetSocketData(const char *parm_address, unsigned short parm_port)
{
	mh_socket = socket(AF_INET, SOCK_STREAM, 0);
	
	strcpy_s(m_server_ip, 24, parm_address);
	m_port = parm_port;
}


void JHClientSocket::Connect()
{
	struct sockaddr_in srv_addr;
	memset(&srv_addr, 0, sizeof(struct sockaddr_in));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr(m_server_ip);
	srv_addr.sin_port = htons(m_port);

	// connect 하면 커넥트 결과가 있을 때까지 30초 보류되어 있다가 빠져나온다
	// 비동기에 의해 결과가 오면 빠져나온다
	// 접속 안됐다 / 됐다 / 접속 시도중
	
	WSAAsyncSelect(mh_socket, m_hWnd, LM_SOCKET_MESSAGE, FD_CONNECT);
	connect(mh_socket, (LPSOCKADDR)&srv_addr, sizeof(struct sockaddr_in));
}


BEGIN_MESSAGE_MAP(JHClientSocket, CDialog)
	ON_MESSAGE(LM_SOCKET_MESSAGE, OnSocketProcess)
END_MESSAGE_MAP()


LRESULT JHClientSocket::OnSocketProcess(WPARAM wParam, LPARAM lParam)
{
	SOCKET cur_socket = (SOCKET)wParam;

	int event_type = WSAGETSELECTEVENT(lParam);
	if (event_type == FD_READ){
		ProcessReadMessage(cur_socket);
	}
	else if (event_type == FD_CONNECT){
		if (0 == WSAGETSELECTERROR(lParam)){
			ProcessConnectMessage(cur_socket);
			UserProcessAfterConnect();
		}
		else {
			UserProcessAfterDissConnect();
		}
		
	}
	else if (event_type == FD_CLOSE){
		ProcessCloseMessage(cur_socket);
	}
	else return 0;

	return 1;
}


char JHClientSocket::ProcessConnectMessage(SOCKET parm_socket)
{
	if (0 == WSAAsyncSelect(mh_socket, m_hWnd, LM_SOCKET_MESSAGE, FD_READ | FD_CLOSE)){
		m_socket_flag = 1;
		if (NULL != mp_list_box){
			CString str;
			str.Format(L"서버와 접속되었습니다.");
			mp_list_box->InsertString(-1, str);
		}
		return 1;
	}
	return 0;

}


char JHClientSocket::ReadBodyData(SOCKET parm_socket, int parm_size, char *parm_body_data)
{
	if (parm_body_data != NULL) {
		char retry_count = 0;
		int total_read_size = 0, read_size = 0;

		while (total_read_size < parm_size){
			read_size = recv(parm_socket, parm_body_data + total_read_size, parm_size - total_read_size, 0);
			if (0 == read_size || SOCKET_ERROR == read_size){
				retry_count++;
				if (retry_count >= 500) {
					AfxMessageBox(L"EEEEEE!!!");
					return NULL;
				}
				else Sleep(50);
			}
			else{
				total_read_size += read_size;
				retry_count = 0;
			}
		} // 데이터를 읽지 못한 경우
		return 1;

	}
	return 0;
}


int JHClientSocket::SendFrameData(SOCKET parm_socket, unsigned char parm_message_id, void *parm_data, int parm_send_size)
{
	char *p_send_data = new char[1 + 1 + 4 + parm_send_size];
	p_send_data[0] = KEY_VALUE;
	p_send_data[1] = parm_message_id;
	*(int *)(p_send_data + 2) = parm_send_size;

	if (0 != parm_send_size){
		memcpy(p_send_data + 6, parm_data, parm_send_size);
	}

	int send_size = send(parm_socket, p_send_data, 6 + parm_send_size, 0);
	delete[] p_send_data;
	return send_size;
//	char retry_count = 0;
//	int send_size = 0, total_send_size = 0;
//	while (total_send_size < parm_send_size){
//		send_size = send(parm_socket, p_send_data, 6 + parm_send_size - total_send_size, 0);
//		if (send_size == SOCKET_ERROR || send_size <= 6){
//			retry_count++;
//			if (retry_count >= 5) {
//				int d = 0;
//				d++;
//			} 
//		}
//		else {
//			total_send_size += send_size - 6;
//			retry_count = 0;
//		}
//		
//	}
//
//	delete[] p_send_data;
//	return send_size;
}

// 서버의 소켓을 명시하지 않아도 되는 SendFrameData
int JHClientSocket::SendFrameData(unsigned char parm_message_id, void *parm_data, int parm_send_size)
{
	return SendFrameData(mh_socket, parm_message_id, parm_data, parm_send_size);
}



char JHClientSocket::ProcessReadMessage(SOCKET parm_socket)
{
	char result = WSAAsyncSelect(parm_socket, m_hWnd, LM_SOCKET_MESSAGE, FD_CLOSE);
	if (result == 0){
		char key;
		recv(parm_socket, &key, 1, 0);
		if (KEY_VALUE == key){
			unsigned char message_id;
			if (recv(parm_socket, (char *)&message_id, 1, 0)){
				// data 길이
				unsigned int data_size = 0;
				if (recv(parm_socket, (char *)&data_size, 4, 0)){
					if (0 < data_size){
						char *p_data = new char[data_size];
						if (ReadBodyData(parm_socket, data_size, p_data)){
							UserProcessReadData(message_id, data_size, p_data);
						}
						else ProcessCloseMessage(parm_socket);
						delete[] p_data;
					}
					else {
						UserProcessReadData(message_id, NULL, 0);
					}
					if (0 != WSAAsyncSelect(parm_socket, m_hWnd, LM_SOCKET_MESSAGE, FD_CLOSE | FD_READ)){
						// FD_CLOSE | FD_READ 비동기에 실패한 경우
						return -7;
					}
				} // 데이터 사이즈를 얻지 못한 경우
				else return -5;

			} // 메시지 아이디 못 받은 경우
			else return -3;
		}
		else {
			ProcessCloseMessage(parm_socket);
			// 잘못된 key 값을 전송한 클라이언트의 경우
			return -4;
		}
	} // fd_close 비동기에 실패한 경우
	else return -1;

	return 1;
}

char JHClientSocket::ProcessCloseMessage(SOCKET parm_socket)
{
	char result = DestroySocket(mh_socket);
	mh_socket = INVALID_SOCKET;

	if (NULL != mp_list_box){
		CString str;
		str.Format(L"서버와 접속이 해제되었습니다.");
		mp_list_box->InsertString(-1, str);
	}
	return result;
}




char JHClientSocket::DestroySocket(SOCKET parm_socket)
{
	if (INVALID_SOCKET == parm_socket) return -1;
	LINGER l_linger = { TRUE, 0 };
	setsockopt(parm_socket, SOL_SOCKET, SO_LINGER, (char FAR *)&l_linger, sizeof(l_linger));
	closesocket(parm_socket);
	m_socket_flag = 0;
	return 1;
}



JHClientSocket::~JHClientSocket()
{
	DestroySocket(mh_socket);
}


