
// JH_PrimeDlg.h : 헤더 파일
//
#include "JHClientSocket.h"
#include "JHXps.h"
#pragma once


// CJH_PrimeDlg 대화 상자
class CJH_PrimeDlg : public JHClientSocket
{
private:

    CDC *mp_mem_dc;


public:
    CJH_PrimeDlg(CWnd* pParent = NULL); // 표준 생성자입니다.
    void UserProcessReadData(unsigned char parm_message_id, unsigned int parm_data_size, void *parm_data);

    void MakeXpsToPng(JHXpsManager *parm_xps_manager);

// 대화 상자 데이터입니다.
    enum { IDD = IDD_JH_PRIME_DIALOG };

    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.


// 구현입니다.
protected:
    HICON m_hIcon;
    CListBox m_event_list;

    // 생성된 메시지 맵 함수
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnDropFiles(HDROP hDropInfo);
    afx_msg void OnDestroy();
    afx_msg void OnBnClickedSendBtn();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
};