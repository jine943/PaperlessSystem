#pragma once
#include "afxwin.h"
#include "afxext.h"


// BumBleBeeListDlg 대화 상자입니다.

class BumBleBeeListDlg : public CDialogEx
{
	DECLARE_DYNAMIC(BumBleBeeListDlg)
private:
	int m_bbb_count;
	BBB_State *mp_bbb_list;
	wchar_t m_selected_id[32];

public:
	BumBleBeeListDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~BumBleBeeListDlg();

	void SetBBB(int parm_count, BBB_State *parm_p_list);
	wchar_t *GetSelectedID();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_bbb_list;
	afx_msg void OnBnClickedNonSelectBtn();
	afx_msg void OnDblclkBumblebeeList();
public:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnBnClickedCancal();
	CBitmapButton m_cancel_btn;
	afx_msg void OnPaint();
	CBitmapButton m_no_select_btn;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
