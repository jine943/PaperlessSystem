#include "stdafx.h"
#include "JHXps.h"

IXpsOMObjectFactory *gp_factory;


//////////////////////////////////////////////////////////////
////////////////////////// JHXpsManager //////////////////////

JHXpsManager::JHXpsManager(wchar_t *parm_xps_file_name)
{
	m_doc_count = 0;

	HRESULT hr = CoInitializeEx(0, COINITBASE_MULTITHREADED);
	if (SUCCEEDED(hr)){
		hr = CoCreateInstance(__uuidof(XpsOMObjectFactory), NULL, CLSCTX_INPROC_SERVER, __uuidof(IXpsOMObjectFactory), reinterpret_cast<LPVOID*>(&mp_factory));
		if (SUCCEEDED(hr)){
			hr = mp_factory->CreatePackageFromFile(parm_xps_file_name, TRUE, &mp_package);// _T(".\\xps\\11st.xps")


			if (SUCCEEDED(hr)){
				gp_factory = mp_factory;
				IXpsOMDocumentSequence *pXpsDocSeq = NULL;
				hr = mp_package->GetDocumentSequence(&pXpsDocSeq);
				if (SUCCEEDED(hr)){
					IXpsOMDocumentCollection *pXpsDocCollection = NULL;
					hr = pXpsDocSeq->GetDocuments(&pXpsDocCollection);
					if (SUCCEEDED(hr)){
						UINT32	doc_count;
						hr = pXpsDocCollection->GetCount(&doc_count);
						if (SUCCEEDED(hr) && 0 < doc_count){
							mp_document_list = new JHXpsDocument *[doc_count];
							for (UINT32 i = 0; i < doc_count; i++){
								IXpsOMDocument *pXpsDocument = NULL;
								hr = pXpsDocCollection->GetAt(i, &pXpsDocument);
								if (SUCCEEDED(hr)){
									mp_document_list[i] = new JHXpsDocument(pXpsDocument);
									
									pXpsDocument->Release();
								} else mp_document_list[i] = NULL;
							}
							m_doc_count = doc_count;
						}
						else {
							mp_document_list = NULL;
						}
						pXpsDocCollection->Release();
					}
					pXpsDocSeq->Release();
				}
				mp_package->Release(); 
			}
		}
	}
}

unsigned int JHXpsManager::GetDocCount()
{
	return m_doc_count;
}

unsigned int JHXpsDocument::GetTotalPage()
{
	return m_total_page_count;
}

JHXpsDocument *JHXpsManager::GetDocument(UINT32 parm_index)
{
	return mp_document_list[parm_index];
}

JHXpsManager::~JHXpsManager()
{
	if (NULL != mp_document_list){
		for (UINT32 i = 0; i < m_doc_count; i++)
			if (NULL != mp_document_list[i]) delete mp_document_list[i];
		
		delete[] mp_document_list;
	}

	if (NULL != mp_factory) mp_factory->Release();
}


//////////////////////////////////////////////////////////////
////////////////////////// JHXpsDocument //////////////////////

JHXpsDocument::JHXpsDocument(IXpsOMDocument *parm_document)
{
	IXpsOMPageReferenceCollection *pXpsPageRefCollection = NULL;
	
	HRESULT hr = parm_document->GetPageReferences(&pXpsPageRefCollection);
	unsigned int page_count;
	if (SUCCEEDED(hr)){
		hr = pXpsPageRefCollection->GetCount(&page_count);
		if (SUCCEEDED(hr)){
			if (0 != page_count){
				mp_page_list = new JHXpsPage *[page_count];

				for (unsigned int i = 0; i < page_count; i++){
					IXpsOMPageReference *p_xps_page_ref;
					hr = pXpsPageRefCollection->GetAt(i, &p_xps_page_ref);
					if (SUCCEEDED(hr)){
						mp_page_list[i] = new JHXpsPage(p_xps_page_ref);
						p_xps_page_ref->Release();
					}
					else mp_page_list[i] = NULL;
				}
				m_total_page_count = page_count;
			}
		}
		pXpsPageRefCollection->Release();
	}
}

JHXpsDocument::~JHXpsDocument()
{
	if (NULL != mp_page_list){
		for (UINT32 i = 0; i < m_total_page_count; i++){
			if (NULL != mp_page_list[i]) delete mp_page_list[i];
		}
		delete[] mp_page_list;
	}
}


JHXpsPage *JHXpsDocument::GetPage(unsigned int parm_page_index)
{
	return mp_page_list[parm_page_index];
}


void JHXpsDocument::DrawPage(unsigned int parm_index, CDC *parm_dc)
{
	if (parm_index >= 0 && parm_index < m_total_page_count){
		if (NULL != mp_page_list[parm_index]) mp_page_list[parm_index]->DrawPage(parm_dc);
	}
}


//////////////////////////////////////////////////////////////
////////////////////////// PAGE //////////////////////

JHXpsPage::JHXpsPage(IXpsOMPageReference *parm_page_ref)
{
	m_width = 800; // default
	m_height = 50;
	IXpsOMPage *p_xps_page;
	HRESULT hr = parm_page_ref->GetPage(&p_xps_page);
	XPS_SIZE xps_rect;

	if (SUCCEEDED(hr)){
		IXpsOMVisualCollection *p_visual_list;
		hr = p_xps_page->GetVisuals(&p_visual_list);
		
		p_xps_page->GetPageDimensions(&xps_rect);
		m_width = xps_rect.width;
		m_height = xps_rect.height;

		hr = p_xps_page->GetVisuals(&p_visual_list);
		if (SUCCEEDED(hr)){
			m_visual_list_count = 0;
			UINT32 visual_count = 0;
			hr = p_visual_list->GetCount(&visual_count);
			if (0 < visual_count){
				if (SUCCEEDED(hr)){
					mp_visual_list = new JHXpsVisual *[visual_count];
					for (UINT32 i = 0; i < visual_count; i++){
						IXpsOMVisual *p_visual = NULL;
						hr = p_visual_list->GetAt(i, &p_visual);
						if (SUCCEEDED(hr) && NULL != p_visual){
							XPS_OBJECT_TYPE object_type;
							hr = p_visual->GetType(&object_type);
							if (SUCCEEDED(hr)){
								if (XPS_OBJECT_TYPE_CANVAS == object_type){
									mp_visual_list[i] = new JHXpsCanvas((IXpsOMCanvas *)p_visual);
								}
								else if (XPS_OBJECT_TYPE_PATH == object_type){
									mp_visual_list[i] = new MyPath((IXpsOMPath *)p_visual);
								}
								else if (XPS_OBJECT_TYPE_GLYPHS == object_type){
									mp_visual_list[i] = new JHXpsGlyphs((IXpsOMGlyphs *)p_visual);
								}
							}
							else {
								mp_visual_list[i] = NULL;
							}
							p_visual->Release();
						}
					}
				}
				m_visual_list_count = visual_count;
			} else {
				mp_visual_list = NULL;
				m_visual_list_count = 0;
			}
			p_visual_list->Release();
		}
		p_xps_page->Release();
	}
}

void JHXpsPage::DrawPage(CDC *parm_dc)
{
	for (unsigned int i = 0; i < m_visual_list_count; i++){
		if (NULL != mp_visual_list[i]) mp_visual_list[i]->Draw(parm_dc);
	}
}


JHXpsPage::~JHXpsPage()
{
	if (NULL != mp_visual_list){
		for (UINT32 i = 0; i < m_visual_list_count; i++){
			if (NULL != mp_visual_list[i]) delete mp_visual_list[i];
		}
		delete[] mp_visual_list;
	}

}

float JHXpsPage::GetWidth()
{
	return m_width;
}

float JHXpsPage::GetHeight()
{
	return m_height;
}
//////////////////////////////////////////////////////////////
////////////////////////// CANVAS////// //////////////////////

JHXpsCanvas::JHXpsCanvas(IXpsOMCanvas *parm_canvas)
{
	m_visual_type = XPS_OBJECT_TYPE_CANVAS;
	// 참고 url : http://www.ecma-international.org/activities/XML%20Paper%20Specification/XPS%20Standard%20WD%201.6.pdf
	IXpsOMVisualCollection *p_visual_list;
	HRESULT hr = parm_canvas->GetVisuals(&p_visual_list);
	if (SUCCEEDED(hr)){
		m_visual_list_count = 0;
		UINT32 visual_count = 0;
		hr = p_visual_list->GetCount(&visual_count);
		if (SUCCEEDED(hr) && 0 < visual_count){
			mp_visual_list = new JHXpsVisual *[visual_count];
			for (UINT32 i = 0; i < visual_count; i++){
				IXpsOMVisual *p_visual = NULL;
				hr = p_visual_list->GetAt(i, &p_visual);
				if (SUCCEEDED(hr) && NULL != p_visual){
					XPS_OBJECT_TYPE visual_type;
					hr = p_visual->GetType(&visual_type);
					if (SUCCEEDED(hr)){
						if (XPS_OBJECT_TYPE_CANVAS == visual_type){
							mp_visual_list[i] = new JHXpsCanvas((IXpsOMCanvas *)p_visual);
						}
						else if (XPS_OBJECT_TYPE_PATH == visual_type){
							mp_visual_list[i] = new MyPath((IXpsOMPath *)p_visual);
						}
						else if (XPS_OBJECT_TYPE_GLYPHS == visual_type){
							mp_visual_list[i] = new JHXpsGlyphs((IXpsOMGlyphs *)p_visual);
						}
					}
					else {
						mp_visual_list[i] = NULL;
					}
					p_visual->Release();
				}
			}
			m_visual_list_count = visual_count;
			p_visual_list->Release();
		} else mp_visual_list = NULL;
	}
}

void JHXpsCanvas::Draw(CDC *parm_dc)
{
	for (UINT32 i = 0; i < m_visual_list_count; i++){
		if (NULL != mp_visual_list[i]) mp_visual_list[i]->Draw(parm_dc);
	}	
}

JHXpsCanvas::~JHXpsCanvas()
{
	if (NULL != mp_visual_list){
		for (UINT32 i = 0; i < m_visual_list_count; i++){
			if (NULL != mp_visual_list[i]) delete mp_visual_list[i];
		}
		delete[] mp_visual_list;
	}
}


//////////////////////////////////////////////////////////////
///////////////////////////// PATH //////////////////////////



MySolidColorBrush::MySolidColorBrush()
{
	m_brush_type = XPS_OBJECT_TYPE_SOLID_COLOR_BRUSH;
}

MySolidColorBrush::MySolidColorBrush(IXpsOMBrush *parm_brush)
{
	m_brush_type = XPS_OBJECT_TYPE_SOLID_COLOR_BRUSH;
	CreateBrush(parm_brush);
}

HRESULT MySolidColorBrush::CreateBrush(IXpsOMBrush *parm_brush)
{
	IXpsOMSolidColorBrush *p_solid_color_brush = (IXpsOMSolidColorBrush *)parm_brush;

	IXpsOMColorProfileResource *p_color_profile_resource;
	XPS_COLOR color;
	HRESULT hr;

	hr = p_solid_color_brush->GetColor(&color, &p_color_profile_resource);
	if (SUCCEEDED(hr)){
		switch (color.colorType)
		{
		case XPS_COLOR_TYPE_SRGB:
			m_brush_color = RGB(color.value.sRGB.red, color.value.sRGB.green, color.value.sRGB.blue);
			break;
		case XPS_COLOR_TYPE_SCRGB:
			m_brush_color = RGB(color.value.scRGB.red, color.value.scRGB.green, color.value.scRGB.blue);
			break;
		}
	}
	return hr;
}

COLORREF MySolidColorBrush::GetColor()
{
	return m_brush_color;
}

MyImageBrush::MyImageBrush()
{
	m_brush_type = XPS_OBJECT_TYPE_IMAGE_BRUSH;
}

MyImageBrush::MyImageBrush(IXpsOMBrush *parm_brush)
{
	m_brush_type = XPS_OBJECT_TYPE_IMAGE_BRUSH;
	CreateBrush(parm_brush);
}

void MyImageBrush::SetRect(int parm_x, int parm_y)
{
	m_rect.left += parm_x;
	m_rect.top += parm_y;
	m_rect.right = parm_x + m_image.GetWidth();
	m_rect.bottom = parm_y + m_image.GetHeight();
}

void MyImageBrush::DrawImage(CDC *parm_dc)
{
	m_rect.SetRect(m_rect.left, m_rect.top, m_rect.left + m_viewport.width, m_rect.top + m_viewport.height);
	m_image.Draw(parm_dc->m_hDC, m_rect);
}

HRESULT MyImageBrush::CreateBrush(IXpsOMBrush *parm_brush)
{
	IXpsOMImageBrush *p_image_brush = (IXpsOMImageBrush *)parm_brush;

	IXpsOMImageResource *p_image_resource;
	XPS_IMAGE_TYPE image_type;
	IStream *p_stream;
	HRESULT hr;
	POINT p_top_left;
	POINT p_bottom_right;
	p_image_brush->GetViewport(&m_viewport);

	hr = p_image_brush->GetImageResource(&p_image_resource);
	if (SUCCEEDED(hr)){
		hr = p_image_resource->GetImageType(&image_type);
		if (SUCCEEDED(hr)){
			hr = p_image_resource->GetStream(&p_stream);
			if (SUCCEEDED(hr)){
				hr = m_image.Load(p_stream);
			}
		}
	}
	return hr;
}

MyFigure::MyFigure()
{
	mp_pos_list = NULL;
	m_pos_count = 0;

	m_is_filled = 0;
	m_is_closed = 0;
}

MyFigure::MyFigure(IXpsOMGeometryFigure *parm_figure)
{
	mp_pos_list = NULL;
	m_pos_count = 0;

	m_is_filled = 0;
	m_is_closed = 0;

	CreateFigure(parm_figure);
}

MyFigure::~MyFigure()
{
	if (mp_pos_list != NULL) delete[] mp_pos_list;
	if (mp_type_array != NULL) delete[] mp_type_array;
}

HRESULT MyFigure::CreateFigure(IXpsOMGeometryFigure *parm_figure)
{
	HRESULT hr;
	BOOL flag;

	hr = parm_figure->GetIsClosed(&flag);
	if (SUCCEEDED(hr)) m_is_closed = flag;

	hr = parm_figure->GetIsFilled(&flag);
	if (SUCCEEDED(hr)) m_is_filled = flag;

	return hr;
}

char MyFigure::GetIsFilled()
{
	return m_is_filled;
}

char MyFigure::GetIsClosed()
{
	return m_is_closed;
}


int MyFigure::GetTypeCount()
{
	return m_type_count;
}

char *MyFigure::GetTypeData()
{
	return mp_type_array;
}
void MyFigure::SetTypeData(int a_type_count, char *ap_type_array)
{
	if (0 != a_type_count){
		m_type_count = a_type_count;
		mp_type_array = new char[m_type_count];
		memcpy(mp_type_array, ap_type_array, sizeof(m_type_count));
	}
}

void MyFigure::BuildPosObject(POINT *parm_pos_list, int parm_pos_count)
{
	if (parm_pos_count > 0){
		mp_pos_list = new POINT[parm_pos_count];
		memcpy(mp_pos_list, parm_pos_list, sizeof(POINT) * parm_pos_count);
	}
	else mp_pos_list = NULL;
	m_pos_count = parm_pos_count;
}

POINT *MyFigure::GetPosList()
{
	return mp_pos_list;
}

int MyFigure::GetPosCount()
{
	return m_pos_count;
}

MyPath::MyPath()
{
	 m_visual_type = XPS_OBJECT_TYPE_PATH;

	mp_brush = NULL;
	mp_figure_list = NULL;
	m_figure_count = 0;
}

MyPath::MyPath(IXpsOMVisual *parm_visual)
{
	m_visual_type = XPS_OBJECT_TYPE_PATH;
	m_figure_count = 0;
	mp_figure_list = NULL;
	mp_brush = NULL;

	Create(parm_visual);
}

MyPath::~MyPath()
{
	if (mp_brush != NULL) delete mp_brush;

	if (mp_figure_list != NULL){
		for (int i = 0; i < m_figure_count; i++){
			if (mp_figure_list[i] != NULL){
				delete mp_figure_list[i];
			}
		}
		delete[] mp_figure_list;
	}
}

HRESULT MyPath::GetMyGeometry(IXpsOMGeometry * parm_geometry)
{

	IXpsOMGeometryFigureCollection *p_figure_list;
	IXpsOMGeometryFigure *p_figure;

	FLOAT *p_segment_data_point_array;
	unsigned int segment_count, segment_data_point_count, figure_count;
	XPS_SEGMENT_TYPE *p_segment_types;

	HRESULT hr = parm_geometry->GetFigures(&p_figure_list);

	if (SUCCEEDED(hr)){
		hr = p_figure_list->GetCount(&figure_count);
		if (SUCCEEDED(hr)){
			if (figure_count > 0){
				m_figure_count = figure_count;
				mp_figure_list = new MyFigure *[figure_count];
			}

			for (unsigned int i = 0; i < figure_count; i++){
				hr = p_figure_list->GetAt(i, &p_figure);
				if (SUCCEEDED(hr)){
					mp_figure_list[i] = new MyFigure(p_figure);

					unsigned int cur_segment_count = 0;
					XPS_POINT start_point;
					POINT temp_pos[500];

					hr = p_figure->GetStartPoint(&start_point);
					if (SUCCEEDED(hr)){
						temp_pos[0].x = (LONG)start_point.x;
						temp_pos[0].y = (LONG)start_point.y;
						cur_segment_count++;

						// 1025 다시 보기 
						if (i == 0 && mp_brush != NULL && mp_brush->GetBrushType() == XPS_OBJECT_TYPE_IMAGE_BRUSH) {
							mp_brush->SetRect((LONG)start_point.x, (LONG)start_point.y);
						}

						hr = p_figure->GetSegmentCount(&segment_count);
						if (SUCCEEDED(hr) && segment_count > 0){
							p_segment_types = new XPS_SEGMENT_TYPE[segment_count];
							hr = p_figure->GetSegmentTypes(&segment_count, p_segment_types);
							char *p_temp_type_array = new char[segment_count];
							int temp_type_count = 0;

							if (SUCCEEDED(hr)){
								hr = p_figure->GetSegmentDataCount(&segment_data_point_count);
								if (SUCCEEDED(hr) && segment_data_point_count > 0){
									p_segment_data_point_array = new FLOAT[segment_data_point_count];
									hr = p_figure->GetSegmentData(&segment_data_point_count, p_segment_data_point_array);
									if (SUCCEEDED(hr)){
										XPS_SEGMENT_TYPE *p_segment_type = p_segment_types;
										XPS_SEGMENT_TYPE *p_last_segment_type = p_segment_types + segment_count;

										FLOAT *p_segment_data_point = p_segment_data_point_array;
										FLOAT *p_last_segment_data_point = p_segment_data_point_array + segment_data_point_count;

										while (cur_segment_count < segment_count + 1){
											if (p_segment_type >= p_last_segment_type || p_segment_data_point >= p_last_segment_data_point) break;

											switch (*p_segment_type)
											{
											case XPS_SEGMENT_TYPE_ARC_LARGE_CLOCKWISE:
											case XPS_SEGMENT_TYPE_ARC_LARGE_COUNTERCLOCKWISE:
											case XPS_SEGMENT_TYPE_ARC_SMALL_CLOCKWISE:
											case XPS_SEGMENT_TYPE_ARC_SMALL_COUNTERCLOCKWISE:
											{
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;

												p_segment_data_point += 3;
												temp_pos[cur_segment_count].y = (LONG)*p_segment_data_point++;
											}
												break;
											case XPS_SEGMENT_TYPE_BEZIER: // 추가할 것 
											{
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;
												// p_segment_data_point += 4;
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;
												// 떨려
												temp_pos[cur_segment_count].y = (LONG)*p_segment_data_point++;
												p_temp_type_array[temp_type_count] = 0;
												temp_type_count++;
											}

												break;
											case XPS_SEGMENT_TYPE_LINE:
											{
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;
												temp_pos[cur_segment_count].y = (LONG)*p_segment_data_point++;
												p_temp_type_array[temp_type_count] = 0;
												temp_type_count++;
											}
												break;
											case XPS_SEGMENT_TYPE_QUADRATIC_BEZIER:
											{
												temp_pos[cur_segment_count].x = (LONG)*p_segment_data_point++;
												p_segment_data_point += 2;

												temp_pos[cur_segment_count].y = (LONG)*p_segment_data_point++;
											}
												break;
											}
											cur_segment_count++;
											p_segment_type++;
										}

										// type 저장
										mp_figure_list[i]->SetTypeData(temp_type_count, p_temp_type_array);

										if (mp_figure_list[i]->GetIsClosed()){
											temp_pos[cur_segment_count] = temp_pos[0];
											cur_segment_count++;
										}
										mp_figure_list[i]->BuildPosObject(temp_pos, cur_segment_count);
									}
								}
								delete[] p_segment_data_point_array;
							}
							delete[] p_segment_types;
						}
					}
					p_figure->Release();
				}
				else mp_figure_list[i] = NULL;
			}
		}
		p_figure_list->Release();
	}
	return hr;

}

HRESULT MyPath::Create(IXpsOMVisual *parm_visual)
{
	IXpsOMPath *p_path = (IXpsOMPath *)parm_visual;
	IXpsOMGeometry *p_geometry;
	IXpsOMBrush *p_brush;

	XPS_OBJECT_TYPE object_type;
	HRESULT hr;

	hr = p_path->GetFillBrush(&p_brush);
	if (SUCCEEDED(hr) && p_brush != NULL){
		hr = p_brush->GetType(&object_type);
		if (SUCCEEDED(hr)){
			if (object_type == XPS_OBJECT_TYPE_IMAGE_BRUSH){
				mp_brush = new MyImageBrush(p_brush);
			}
			else if (object_type == XPS_OBJECT_TYPE_SOLID_COLOR_BRUSH){
				mp_brush = new MySolidColorBrush(p_brush);
			}
			p_brush->Release();
		}
	}
	hr = p_path->GetGeometry(&p_geometry);
	if (SUCCEEDED(hr)){
		GetMyGeometry((IXpsOMGeometry *)p_geometry);
		p_geometry->Release();
	}
	return hr;
}

void MyPath::Draw(CDC *parm_dc)
{
	CBrush *p_old_brush = NULL, fill_brush;
	CPen *p_old_pen = NULL, border_pen;
	char *p_type_data = NULL;
	POINT *p_pos;
	int count;

	if (mp_brush == NULL){
		p_old_brush = (CBrush *)parm_dc->SelectStockObject(WHITE_BRUSH);
		p_old_pen = (CPen *)parm_dc->SelectStockObject(WHITE_PEN);
	}
	else if (mp_brush->GetBrushType() == XPS_OBJECT_TYPE_SOLID_COLOR_BRUSH){
		fill_brush.CreateSolidBrush(mp_brush->GetColor());
		border_pen.CreatePen(PS_SOLID, 1, mp_brush->GetColor());

		p_old_brush = parm_dc->SelectObject(&fill_brush);
		p_old_pen = parm_dc->SelectObject(&border_pen);
	}

	for (int i = 0; i < m_figure_count; i++){
		p_pos = mp_figure_list[i]->GetPosList();
		count = mp_figure_list[i]->GetPosCount();
		
		if (mp_figure_list[i]->GetIsClosed()){
			parm_dc->Polygon(p_pos, count);
		} else {
			parm_dc->MoveTo(p_pos[0]);
			for (int loop = 1; loop < count; loop++) {
				parm_dc->LineTo(p_pos[loop]);
			}
			/*
			p_type_data = mp_figure_list[i]->GetTypeData();
			int type_count = mp_figure_list[i]->GetTypeCount();
			int point = 0;
			parm_dc->MoveTo(p_pos[0]);
			for (int j = 0; j < type_count; j++){
				if (1 == p_type_data[j]){
					parm_dc->PolyBezierTo(&p_pos[point], 3);
					point += 3;
				} else parm_dc->LineTo(p_pos[point]);
			}

			for (int loop = 1; loop < count; loop++) {
				
			}
			*/
		}
	}
	if (mp_brush != NULL && mp_brush->GetBrushType() == XPS_OBJECT_TYPE_IMAGE_BRUSH){
		mp_brush->DrawImage(parm_dc);
	}
	if (p_old_brush != NULL) parm_dc->SelectObject(p_old_brush);
	if (p_old_pen != NULL) parm_dc->SelectObject(p_old_pen);
	fill_brush.DeleteObject();
	border_pen.DeleteObject();
}

//////////////////////////////////////////////////////////////
//////////////////////////// GLYPHS //////////////////////////

JHXpsGlyphs::JHXpsGlyphs(IXpsOMGlyphs *parm_glyphs)
{
	m_visual_type = XPS_OBJECT_TYPE_GLYPHS;
	m_text_style = XPS_STYLE_SIMULATION_NONE;

	m_font_size = 96;
	m_text_color = RGB(0, 0, 0);

	LPWSTR p_string = NULL;
	LPWSTR p_font_name = NULL;
	HRESULT hr = parm_glyphs->GetUnicodeString(&p_string);
	if (SUCCEEDED(hr)){
		if (NULL != p_string){
			FLOAT *p_font_render_size = NULL;
			XPS_POINT temp_seg_pos;
			hr = parm_glyphs->GetOrigin(&temp_seg_pos);

			if (SUCCEEDED(hr)){
				m_x = (int)temp_seg_pos.x;
				m_y = (int)temp_seg_pos.y;

				m_string = p_string;
				parm_glyphs->GetFontRenderingEmSize(&m_font_size);
				parm_glyphs->GetDeviceFontName(&p_font_name);
				mp_face_name.SetString(p_font_name);

				m_y -= m_font_size;

				IXpsOMBrush *p_brush;
				MySolidColorBrush *p_solid_color_brush;
				XPS_OBJECT_TYPE object_type;

				hr = parm_glyphs->GetFillBrush(&p_brush);
				if (SUCCEEDED(hr) && p_brush != NULL){
					hr = p_brush->GetType(&object_type);
					if (SUCCEEDED(hr)){
						if (object_type == XPS_OBJECT_TYPE_SOLID_COLOR_BRUSH){
							p_solid_color_brush = new MySolidColorBrush(p_brush);
							m_text_color = p_solid_color_brush->GetColor();
							delete p_solid_color_brush;
						}
						p_brush->Release();
					}
				}

			}
		}
	}
}