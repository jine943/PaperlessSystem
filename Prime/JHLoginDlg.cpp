// JHLoginDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JH_Prime.h"
#include "JHLoginDlg.h"
#include "afxdialogex.h"


extern JHLoginDlg *gp_log_dlg = NULL;

IMPLEMENT_DYNAMIC(JHLoginDlg, CDialogEx)

JHLoginDlg::JHLoginDlg(CWnd* pParent) :CDialogEx(JHLoginDlg::IDD, pParent)
{
	m_click_flag = 0;
}

JHLoginDlg::~JHLoginDlg()
{
}

void JHLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOGO, m_logo_picture);
	DDX_Control(pDX, IDC_BUTTON2, m_close_btn);
	DDX_Control(pDX, IDC_LOGIN_BTN, m_login_btn);
}


BEGIN_MESSAGE_MAP(JHLoginDlg, CDialogEx)
	ON_BN_CLICKED(IDC_LOGIN_BTN, &JHLoginDlg::OnBnClickedLoginBtn)
	ON_BN_CLICKED(IDOK, &JHLoginDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &JHLoginDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON2, &JHLoginDlg::OnBnClickedButton2)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void JHLoginDlg::DecryptionData(unsigned char *parm_data, unsigned int parm_size)
{
	for (unsigned int i = 0; i < parm_size; i++){
		*parm_data = (*parm_data) ^ 0x69;
		parm_data++;
	}
}

void JHLoginDlg::EncryptionData(unsigned char *parm_data, unsigned int parm_size)
{
	for (unsigned int i = 0; i < parm_size; i++){
		*parm_data = (*parm_data) ^ 0x69;
		parm_data++;
	}
}

void JHLoginDlg::SaveLoginData()
{
	CFile login_file;

	if (TRUE == login_file.Open(L"ClientAccount.dat", CFile::modeCreate | CFile::modeWrite)){
		int temp_length = m_id.GetLength() + 1;
		int str_length = temp_length;

		wchar_t *p_temp = new wchar_t[temp_length];
		wcscpy_s(p_temp, temp_length, m_id);

		EncryptionData((unsigned char *)&temp_length, sizeof(int));
		login_file.Write(&temp_length, sizeof(int));
		EncryptionData((unsigned char *)p_temp, str_length * 2);
		login_file.Write(p_temp, str_length * 2);

		delete[] p_temp;

		temp_length = m_pw.GetLength() + 1;
		str_length = temp_length;
		p_temp = new wchar_t[temp_length];
		wcscpy_s(p_temp, temp_length, m_pw);

		EncryptionData((unsigned char *)&temp_length, sizeof(int));
		login_file.Write(&temp_length, sizeof(int));
		EncryptionData((unsigned char *)p_temp, str_length * 2);
		login_file.Write(p_temp, str_length * 2);

		temp_length = m_ip.GetLength() + 1;
		str_length = temp_length;
		p_temp = new wchar_t[temp_length];
		wcscpy_s(p_temp, temp_length, m_ip);

		EncryptionData((unsigned char *)&temp_length, sizeof(int));
		login_file.Write(&temp_length, sizeof(int));
		EncryptionData((unsigned char *)p_temp, str_length * 2);
		login_file.Write(p_temp, str_length * 2);

		delete[] p_temp;
		login_file.Close();
	}
}

void JHLoginDlg::OnBnClickedLoginBtn()
{
	GetDlgItemText(IDC_IP_EDIT, m_ip);
	GetDlgItemText(IDC_ID_EDIT, m_id);
	GetDlgItemText(IDC_PW_EDIT, m_pw);

	if (0 == m_ip.IsEmpty() && 0 == m_id.IsEmpty() && 0 == m_pw.IsEmpty()){
		SaveLoginData();
		CDialogEx::OnOK();
	}
	else MessageBox(L"IP, ID, PW를 정확히 입력해주세요!", L"로그인 실패", MB_OKCANCEL | MB_ICONQUESTION);
}



BOOL JHLoginDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CFile login_file;
	int temp_length;
	//추가
	SetBackgroundColor(RGB(153, 255, 51), 1);
	HBITMAP hbit;
	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_PRIME_LOGO));
	m_logo_picture.SetBitmap(hbit);
	//////////////////
	m_close_btn.LoadBitmaps(IDB_CLOSE, NULL, NULL, NULL);
	m_close_btn.SizeToContent();
	m_login_btn.LoadBitmaps(IDB_LOGIN, NULL, NULL, NULL);
	m_login_btn.SizeToContent();
	
	//추가
	if (TRUE == login_file.Open(L"ClientAccount.dat", CFile::modeRead))
	{
		login_file.Read(&temp_length, sizeof(int));
		DecryptionData((unsigned char *)&temp_length, sizeof(int));

		wchar_t *p_temp = new wchar_t[temp_length];

		login_file.Read(p_temp, temp_length * 2);
		DecryptionData((unsigned char *)p_temp, temp_length * 2);
		m_id = p_temp;

		delete[] p_temp;

		login_file.Read(&temp_length, sizeof(int));
		DecryptionData((unsigned char *)&temp_length, sizeof(int));

		p_temp = new wchar_t[temp_length];

		login_file.Read(p_temp, temp_length * 2);
		DecryptionData((unsigned char *)p_temp, temp_length * 2);
		m_pw = p_temp;

		delete[] p_temp;

		login_file.Read(&temp_length, sizeof(int));
		DecryptionData((unsigned char *)&temp_length, sizeof(int));

		p_temp = new wchar_t[temp_length];

		login_file.Read(p_temp, temp_length * 2);
		DecryptionData((unsigned char *)p_temp, temp_length * 2);
		m_ip = p_temp;

		login_file.Close();
		SetDlgItemText(IDC_ID_EDIT, m_id);
		SetDlgItemText(IDC_PW_EDIT, m_pw);
		SetDlgItemText(IDC_IP_EDIT, m_ip);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void JHLoginDlg::OnBnClickedOk()
{
	OnBnClickedLoginBtn();	
}

void JHLoginDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}

CString JHLoginDlg::GetIP(){
	return m_ip;
}

const wchar_t *JHLoginDlg::GetID()
{
	return m_id;
}

const wchar_t *JHLoginDlg::GetPW()
{
	return m_pw;
}



void JHLoginDlg::OnBnClickedButton2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnBnClickedCancel();
}

////////////여기서 부터
void JHLoginDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_click_flag = 1;
	POINT temp_pos;
	GetCursorPos(&temp_pos);
	m_old_pos = temp_pos;
	SetCapture();

	CDialogEx::OnLButtonDown(nFlags, point);
}


void JHLoginDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_click_flag = 0;
	ReleaseCapture();

	CDialogEx::OnLButtonUp(nFlags, point);
}


void JHLoginDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (m_click_flag == 1){
		CRect r;
		GetWindowRect(r);
		POINT temp_pos;
		GetCursorPos(&temp_pos);
		MoveWindow(r.left + temp_pos.x - m_old_pos.x, r.top + temp_pos.y - m_old_pos.y, r.Width(), r.Height());
		m_old_pos = temp_pos;
	}
	CDialogEx::OnMouseMove(nFlags, point);
}
//// 여기까지

BOOL JHLoginDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	HCURSOR hCursor;
	hCursor = AfxGetApp()->LoadStandardCursor(IDC_HAND);

	if (pMsg->message == WM_MOUSEMOVE){
		CButton *p_ok_btn = (CButton *)GetDlgItem(IDC_BUTTON2);
		CButton *p_login_btn = (CButton *)GetDlgItem(IDC_LOGIN_BTN);
		if (p_ok_btn != NULL && p_ok_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
		
		if (p_login_btn != NULL && p_login_btn->m_hWnd == pMsg->hwnd) SetCursor(hCursor);
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}