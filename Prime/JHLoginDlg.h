#pragma once
#include "JH_PrimeDlg.h"
#include "afxwin.h"
#include "afxext.h"



// JHLoginDlg 대화 상자입니다.

class JHLoginDlg : public CDialogEx
{
	DECLARE_DYNAMIC(JHLoginDlg)
	CString m_ip, m_id, m_pw;
	unsigned short m_id_len, m_pw_len;
	char m_click_flag;
	POINT m_old_pos;
public:
	JHLoginDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~JHLoginDlg();
	CString GetIP();
	const wchar_t *GetID();
	const wchar_t *GetPW();

	void EncryptionData(unsigned char *parm_data, unsigned int parm_size);
	void DecryptionData(unsigned char *parm_data, unsigned int parm_size);
	void SaveLoginData();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LOGIN_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedLoginBtn();
	virtual BOOL OnInitDialog();
//	afx_msg void OnIdok();
//	afx_msg void OnIdcancel();
	afx_msg void OnBnClickedOk();
//	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedCancel();

	afx_msg void OnBnClickedButton2();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

	CStatic m_logo_picture;
	CBitmapButton m_close_btn;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CBitmapButton m_login_btn;
};


extern JHLoginDlg *gp_log_dlg;
