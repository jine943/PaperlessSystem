
// JHFaxDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "JHServerSocket.h"
#include "IpSelectDlg.h"
#include "ExchangeService.h"

#define MAX_PRIME_COUNT			500
#define MAX_BUMBLEBEE_COUNT		500

// 사용자 측 id pw, 주로 사용하는 범블비
// 서버에서만 id pw 등록할 수 있도록 
// CJHFaxDlg 대화 상자
class CJHFaxDlg : public JHServerSocket
{
private:
	SocketData m_prime_list[MAX_PRIME_COUNT];
	SocketData m_bumblebee_list[MAX_BUMBLEBEE_COUNT];
	unsigned int m_prime_count, m_bumblebee_count;

	ExchangeService m_ex_manager;

	// 프라임이 보낼때
	CFile m_recv_file, m_recv_file_by_bbb;

	// CFile m_recv_file_bbb;
	char m_user_count;
	unsigned int m_save_paper_count;
	CFile m_save_paper_count_file;

	BOOL FindSocketIndex(SocketData *parm_list, unsigned int parm_list_count, SOCKET parm_socket, unsigned short *parm_index);
	void ArrangeSocket(SocketData *parm_list, unsigned int *parm_p_list_count, unsigned short parm_socket_index);
// 생성입니다.
public:
	CJHFaxDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

	void AddFaxlistString(const wchar_t *ap_string);
	void AddEventString(const wchar_t *ap_string);
	void BroadCastPrime(unsigned char parm_message_id, void *parm_data, int parm_data_size);
	void BroadCastBumblebee(unsigned char parm_message_id, void *parm_data, int parm_data_size);
	void SaveUserData();
	void LoadUserData();
	void Request_BBB_List(SOCKET parm_socket);
	SocketData *TransferClientType(SOCKET parm_socket, char parm_type);
	SOCKET FindBBBSocket(wchar_t *ap_bbb_name);
	SocketData *FindBBBSocketData(wchar_t *ap_bbb_name);

	void NotifyFavoritBBBStateToPrime(SOCKET ap_bbb_socket, char a_login_info, unsigned int parm_screen_data_size, void *parm_screen_data);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_JHFAX_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.
	virtual void UserProcessReadData(SOCKET parm_socket, unsigned char parm_message_id, unsigned int parm_data_size, void *p_data);
	virtual char ProcessCloseMessage(SOCKET parm_socket);

	char ProcessLoginMessage(SOCKET parm_socket, unsigned int parm_data_size, void *parm_data);

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	
	DECLARE_MESSAGE_MAP()
	
public:
	CListBox m_chat_list;
	afx_msg void OnDestroy();
protected:
//	afx_msg LRESULT OnSocketMessage(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedSendBtn();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
protected:
	afx_msg LRESULT OnLmClientTypeMessage(WPARAM wParam, LPARAM lParam);
private:
	CListBox m_user_list;
public:
	afx_msg void OnBnClickedAddUserBtn();
	afx_msg void OnBnClickedModifyUserBtn();
	afx_msg void OnBnClickedDelUserBtn();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
protected:
//	afx_msg LRESULT OnLmRequestBbbList(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedOk();
//	afx_msg void OnEnChangePaperlessEdit();
	afx_msg void OnEnChangeIpEdit();
	CListBox m_fax_list;
	afx_msg void OnBnClickedBbbSendBtn();
};
