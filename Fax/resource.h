//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// JHFax.rc에서 사용되고 있습니다.
//
#define IDD_JHFAX_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_ADD_MODIFY_USER_DLG         130
#define IDD_IP_SELECT                   131
#define IDC_LIST1                       1000
#define IDC_CHAT_LIST                   1000
#define IDC_BUTTON1                     1001
#define IDC_SEND_BTN                    1001
#define IDC_EDIT1                       1002
#define IDC_CHAT_EDIT                   1002
#define IDC_USER_ID                     1002
#define IDC_REMAIN_TIME_EDIT            1002
#define IDC_USER_LIST                   1003
#define IDC_USER_PASSWORD               1003
#define IDC_ADD_USER_BTN                1004
#define IDC_MODIFY_USER_BTN             1005
#define IDC_PRIME_RADIO                 1005
#define IDC_DEL_USER_BTN                1006
#define IDC_BUMBLEBEE_RADIO             1006
#define IDC_BBB_SEND_BTN                1007
#define IDC_CUR_USER_EDIT               1008
#define IDC_PAPERLESS_EDIT              1009
#define IDC_IP_EDIT                     1010
#define IDC_FAX_LIST                    1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
