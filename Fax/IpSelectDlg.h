#pragma once
#include "afxwin.h"


// IpSelectDlg 대화 상자입니다.

class IpSelectDlg : public CDialogEx
{
	DECLARE_DYNAMIC(IpSelectDlg)
private:
	char m_ip_address[MAX_IP_LEN];

public:
	int GetLocalNetworkAddress(char parm_buffer[][24], int parm_max_count);
	char * GetIP();

	IpSelectDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~IpSelectDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_IP_SELECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
//	afx_msg void OnBnClickedOk();
	afx_msg void OnDblclkList1();
	virtual void OnOK();
	CListBox m_ip_list;
	afx_msg void OnBnClickedCancel();
};
