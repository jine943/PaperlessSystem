
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#define _WINSOCK_DEPRECATED_NO_WARNINGS 

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.


#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#define MAX_PAGE					10
#define	MAX_IP_LEN					24
#define CLIENT_PRIME_TYPE			0
#define CLIENT_BUMBLEBEE_TYPE		1

// 네트워크 메시지
#define NM_CHAT_MESSAGE			 10
#define NM_SEND_TEST_PAGE		 20
#define NM_SEND_PAGE_TO_BBB		 21

#define NM_CLIENT_TYPE_MESSAGE	 30	// Prime : 0 , BumBleBee : 1
#define NM_START_SEND_PAGE		 40
#define NM_LOGIN_MESSAGE	     50
#define NM_LOGIN_SUCCESS_MESSAGE 51
#define NM_LOGIN_FAIL_MESSAGE    52
#define NM_BBB_SCREEN_DATA       53
#define NM_FAVORIT_BBB_DATA      54

#define NM_REQUEST_BBB_LIST		 60
#define NM_SEND_BBB_LIST		 70
#define NM_SELECT_BBB_NAME       71


// prime 부분
#define NM_START_SEND_FILES      80
#define NM_SEND_FILE_NAME        81
#define NM_NEXT_FILE_DATA        82
#define NM_SEND_FILE_DATA        83
#define NM_LAST_FILE_DATA        84
#define NM_END_SEND_FILE         85

// bbb prime 동일하게 사용
#define NM_CANNOT_START_SEND_FILE 86
#define NM_REQUEST_FILE				87


//BBB 에서 FAX로
#define NM_BBB_START_SEND_FILES		 90
#define NM_BBB_SEND_FILE_NAME        91
#define NM_BBB_NEXT_FILE_DATA        92
#define NM_BBB_SEND_FILE_DATA        93
#define NM_BBB_LAST_FILE_DATA        94
#define NM_BBB_END_SEND_FILE         95


// 로컬 메시지
#define LM_SEND_FILE_MESSAGE		28001
#define LM_CLIENT_TYPE_MESSAGE		28002

struct LoginData
{
	wchar_t id[32];
	wchar_t pw[32];
};

struct BBB_State
{
	char state;
	wchar_t name[32];
	int screen[2];
};

struct UserInformation;

struct SocketData {
	SOCKET h_socket;
	char ip[24];
	UserInformation *p_user_info;
	SOCKET h_favorit_socket;
	int screen[2];
};

struct PageName
{
	unsigned short page_name_len;
	wchar_t *p_page_name;
};

struct UserInformation
{
	wchar_t id[32];
	wchar_t password[32];
	char user_type;   // 0 : 프라임, 1 : 범블비
	
	SocketData*p_runtime_info;

	wchar_t favorit_user[32];
//	wchar_t favorit_prime[32];

	PageName page_name[MAX_PAGE];
	CFile *p_page[MAX_PAGE];
	unsigned short page_count;
};




#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


