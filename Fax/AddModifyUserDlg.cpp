// AddModifyUserDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JHFax.h"
#include "AddModifyUserDlg.h"
#include "afxdialogex.h"


// AddModifyUserDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(AddModifyUserDlg, CDialogEx)

AddModifyUserDlg::AddModifyUserDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(AddModifyUserDlg::IDD, pParent)
{

}

AddModifyUserDlg::~AddModifyUserDlg()
{
}

void AddModifyUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(AddModifyUserDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &AddModifyUserDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// AddModifyUserDlg 메시지 처리기입니다.


BOOL AddModifyUserDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (!m_user_id.IsEmpty()){
		SetDlgItemText(IDC_USER_ID, m_user_id);
		((CEdit *)GetDlgItem(IDC_USER_ID))->SetReadOnly();

		SetDlgItemText(IDC_USER_PASSWORD, m_user_password);

		((CButton *)GetDlgItem(IDC_PRIME_RADIO))->SetCheck(!m_type);
		((CButton *)GetDlgItem(IDC_BUMBLEBEE_RADIO))->SetCheck(m_type);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void AddModifyUserDlg::OnBnClickedOk()
{
	GetDlgItemText(IDC_USER_ID, m_user_id);
	GetDlgItemText(IDC_USER_PASSWORD, m_user_password);
	m_type = ((CButton *)GetDlgItem(IDC_BUMBLEBEE_RADIO))->GetCheck();

	CDialogEx::OnOK();
}
