// IpSelectDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JHFax.h"
#include "IpSelectDlg.h"
#include "afxdialogex.h"


// IpSelectDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(IpSelectDlg, CDialogEx)

IpSelectDlg::IpSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IpSelectDlg::IDD, pParent)
{

}

IpSelectDlg::~IpSelectDlg()
{
}

void IpSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ip_list);
}


BEGIN_MESSAGE_MAP(IpSelectDlg, CDialogEx)
	ON_WM_TIMER()
//	ON_BN_CLICKED(IDOK, &IpSelectDlg::OnBnClickedOk)
	ON_LBN_DBLCLK(IDC_LIST1, &IpSelectDlg::OnDblclkList1)
	ON_BN_CLICKED(IDCANCEL, &IpSelectDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


int IpSelectDlg::GetLocalNetworkAddress(char parm_buffer[][24], int parm_max_count)
{
	int count = 0, i;

	IN_ADDR in_address;
	char host_name[128];

	// 현재 컴퓨터의 호스트 명칭을 얻는다.
	int return_val = gethostname(host_name, 128);
	
	if (SOCKET_ERROR == return_val){
		int debeg = 0;
		debeg++;
	}
	// 호스트 데이터베이스에서 지정한 호스트 명칭에 대응하는 호스트 정보를 얻는다.
	HOSTENT *p_host_info = gethostbyname(host_name);
	if (p_host_info != NULL){
		// p_host_info->h_addr_list[i]에 주소가 유효한 경우
		for (i = 0; p_host_info->h_addr_list[i]; i++){
			if (parm_max_count > i){
				// 해당 주소를 복사한다.
				memcpy(&in_address, p_host_info->h_addr_list[i], 4);
				strcpy_s(parm_buffer[i], inet_ntoa(in_address));
			}
			else break;
		}
		count = i;
	}
	return count;// 읽어 들인 IP의 갯수를 반환한다.
}

char *IpSelectDlg::GetIP()
{
	return m_ip_address;
}

BOOL IpSelectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	char ip_address_list[5][24];

	WSADATA data;
	WSAStartup(0x0202, &data);

	int count = GetLocalNetworkAddress(ip_address_list, 5), i = 0;

	WSACleanup();

	for (i = 0; i < count; i++){
		m_ip_list.InsertString(-1, CString(ip_address_list[i]));
	}

	m_ip_list.SetCurSel(0);

	SetDlgItemInt(IDC_REMAIN_TIME_EDIT, 5);

	SetTimer(1, 1000, NULL);


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void IpSelectDlg::OnTimer(UINT_PTR nIDEvent)
{

	if (nIDEvent == 1){
		// 에디트박스에서 숫자를 얻어와 0 이상인 경우 1을 감소시키고
		// 0인 경우 타이머를 종료하고 다이얼로그를 닫는다.
		int remain_time = GetDlgItemInt(IDC_REMAIN_TIME_EDIT);

		if (remain_time > 0){
			SetDlgItemInt(IDC_REMAIN_TIME_EDIT, remain_time - 1);

		}
		else {
			KillTimer(1);
			OnOK();
		}
	}

	CDialog::OnTimer(nIDEvent);
}


//void IpSelectDlg::OnBnClickedOk()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	CDialogEx::OnOK();
//}


void IpSelectDlg::OnDblclkList1()
{
	OnOK();
}


void IpSelectDlg::OnOK()
{
	// 현재 리스트박스의 커서위치를 얻는다.
	int index = m_ip_list.GetCurSel();
	// 유효한 위치인 경우
	CString ip;
	if (index != LB_ERR){
		// 선택된 항목의 문자열을 얻어온다.
		m_ip_list.GetText(index, ip);
		int length = ip.GetLength() + 1;
		for (int i = 0; i < length; i++) m_ip_address[i] = (char)ip[i];
		KillTimer(1);

		CDialog::OnOK();
	}	
}


void IpSelectDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}
