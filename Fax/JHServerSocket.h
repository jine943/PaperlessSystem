#ifndef _JH_SOCKET_
#define _JH_SOCKET_

#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")


#define LM_SOCKET_MESSAGE		20000

#define MAX_CLIENT_COUNT		1000

#define KEY_VALUE				16

class JHServerSocket : public CDialog
{
protected:
	SOCKET mh_listen_socket;

	SOCKET mh_client_socket[MAX_CLIENT_COUNT];
	char mh_client_ip[MAX_CLIENT_COUNT][24];
	// IP 길이는 고정된 정보이므로 함께 저장
	// 서버는 길이를 계산하는 노동을 줄여야 한다
	// 메모리는 많으니까 클라이언트에 대한 정보를 로그인시 모두 계산하여 저장
	unsigned int m_client_count;

	
	char m_ip_address[24];
	unsigned short m_port;

	CListBox *mp_list_box;
	
public:
	JHServerSocket(UINT nIDTemplate, CWnd* pParentWnd = NULL);
	~JHServerSocket();

	void SetSocketData(char *parm_address, unsigned short parm_port);

	void AddEventString(const wchar_t *ap_string);
	void StartListenSocket();	

	char ProcessAcceptMessage(SOCKET parm_socket);
	char ProcessReadMessage(SOCKET parm_socket);
	virtual char ProcessCloseMessage(SOCKET parm_socket);

	
	char DestroySocket(SOCKET parm_socket);
	
	BOOL FindSocketIndex(SOCKET parm_socket, unsigned short *parm_index);
	void ArrangeSocket(unsigned short parm_socket_index, char *parm_ip = NULL);
	virtual void UserProcessReadData(SOCKET parm_socket, unsigned char parm_message_id, unsigned int data_size, void *p_data){ }

	char ReadBodyData(SOCKET parm_socket, int parm_size, char *parm_body_data);
	int SendFrameData(SOCKET parm_socket, unsigned char parm_message_id, void *parm_data, int parm_send_size);
	char BroadCastData(unsigned char parm_message_id, void *parm_data, int parm_data_size);

	DECLARE_MESSAGE_MAP()
	LRESULT OnSocketProcess(WPARAM wParam, LPARAM lParam);
	
};


class JHClientSocket
{
private:
	SOCKET mh_socket;

	char m_server_ip[24];
	unsigned short m_port;
	HWND mh_Wnd;
public:
	JHClientSocket(const char *parm_address, unsigned short parm_port, HWND parm_handle);
	~JHClientSocket();
	void Connect();

	char ProcessReadMessage(WPARAM wParam, LPARAM lParam);
	virtual char ProcessCloseMessage(WPARAM wParam, LPARAM lParam);
	char ProcessConnectMessage(WPARAM wParam, LPARAM lParam);
	
	void SendString(const char *parm_string);
	char DestroySocket(SOCKET parm_socket);
	
	
};


#endif