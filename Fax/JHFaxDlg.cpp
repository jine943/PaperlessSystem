// JHFaxDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "JHFax.h"
#include "JHFaxDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CJHFaxDlg 대화 상자

CJHFaxDlg::CJHFaxDlg(CWnd* pParent) : JHServerSocket(CJHFaxDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_prime_count = 0;
	m_bumblebee_count = 0;
	m_user_count = 0;
	m_save_paper_count = 0;
}

void CJHFaxDlg::DoDataExchange(CDataExchange* pDX)
{
	JHServerSocket::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHAT_LIST, m_chat_list);
	DDX_Control(pDX, IDC_USER_LIST, m_user_list);
	DDX_Control(pDX, IDC_FAX_LIST, m_fax_list);
}

BEGIN_MESSAGE_MAP(CJHFaxDlg, JHServerSocket)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SEND_BTN, &CJHFaxDlg::OnBnClickedSendBtn)
	ON_BN_CLICKED(IDC_ADD_USER_BTN, &CJHFaxDlg::OnBnClickedAddUserBtn)
	ON_BN_CLICKED(IDC_MODIFY_USER_BTN, &CJHFaxDlg::OnBnClickedModifyUserBtn)
	ON_BN_CLICKED(IDC_DEL_USER_BTN, &CJHFaxDlg::OnBnClickedDelUserBtn)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_BN_CLICKED(IDOK, &CJHFaxDlg::OnBnClickedOk)
//	ON_EN_CHANGE(IDC_PAPERLESS_EDIT, &CJHFaxDlg::OnEnChangePaperlessEdit)
ON_EN_CHANGE(IDC_IP_EDIT, &CJHFaxDlg::OnEnChangeIpEdit)
ON_BN_CLICKED(IDC_BBB_SEND_BTN, &CJHFaxDlg::OnBnClickedBbbSendBtn)
END_MESSAGE_MAP()


// CJHFaxDlg 메시지 처리기

BOOL CJHFaxDlg::OnInitDialog()
{
	JHServerSocket::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	m_ex_manager.SetSocketInfo(this);
	
	LoadUserData();
	CString id;
//	char ip[24];
	// 폴더를 만든다
	CreateDirectory(L"png", NULL);

	for (int i = 0; i < m_user_list.GetCount(); i++){
		m_user_list.GetText(i, id);
		CreateDirectory(L"png\\"+id, NULL);
	}

	if (TRUE == m_save_paper_count_file.Open(L"save_paper_count.txt", CFile::modeRead)){
		m_save_paper_count_file.Read(&m_save_paper_count, 1);
		m_save_paper_count_file.Close();
	}

	StartListenSocket();
	//승찬
	// memcpy(ip, m_ip_address, 24);
	CString temp_str_ip(m_ip_address);
	//승찬
	
	mp_list_box = &m_chat_list;
	SetWindowText(L"종이를 아끼는 FAX 시스템 FAXCENT, FAX 서버입니다 ♣");
	SetDlgItemInt(IDC_CUR_USER_EDIT, 0);
	SetDlgItemInt(IDC_PAPERLESS_EDIT, m_save_paper_count);

	SetDlgItemText(IDC_IP_EDIT, temp_str_ip);
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CJHFaxDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		JHServerSocket::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CJHFaxDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CJHFaxDlg::OnDestroy()
{
	int count = m_user_list.GetCount();
	UserInformation *p_data;

	for (int i = 0; i < count; i++){
		p_data = (UserInformation *)m_user_list.GetItemDataPtr(i);
		delete p_data;
	}
	m_save_paper_count_file.Open(L"save_paper_count.txt", CFile::modeCreate | CFile::modeWrite);
	m_save_paper_count_file.Write(&m_save_paper_count,1);
	m_save_paper_count_file.Close();

	JHServerSocket::OnDestroy();
}


void CJHFaxDlg::OnBnClickedSendBtn()
{
	CString str;
	int data_size = GetDlgItemText(IDC_CHAT_EDIT, str);
	if (0 < data_size){
		// BroadCastData(NM_CHAT_MESSAGE, (void *)(const wchar_t *)str, (data_size + 1) * 2);
		BroadCastPrime(NM_CHAT_MESSAGE, (void *)(const wchar_t *)str, (data_size + 1) * 2);
	}
	SetDlgItemText(IDC_CHAT_EDIT, L"");
}
/////////////////////////////보성/////////////////////////////
void CJHFaxDlg::OnBnClickedBbbSendBtn()
{
	CString str;
	int data_size = GetDlgItemText(IDC_CHAT_EDIT, str);
	if (0 < data_size){
		BroadCastBumblebee(NM_CHAT_MESSAGE, (void *)(const wchar_t *)str, (data_size + 1) * 2);
	}
	SetDlgItemText(IDC_CHAT_EDIT, L"");
}
//////////////////////////////////////////////////////////////

void CJHFaxDlg::BroadCastPrime(unsigned char parm_message_id, void *parm_data, int parm_data_size)
{
	for (unsigned int i = 0; i < m_prime_count; i++){
		SendFrameData(m_prime_list[i].h_socket, parm_message_id, parm_data, parm_data_size);
	}
}
/////////////////////////////보성/////////////////////////////
void CJHFaxDlg::BroadCastBumblebee(unsigned char parm_message_id, void *parm_data, int parm_data_size)
{
	for (unsigned int i = 0; i < m_bumblebee_count; i++){
		SendFrameData(m_bumblebee_list[i].h_socket, parm_message_id, parm_data, parm_data_size);
	}
}
//////////////////////////////////////////////////////////////

SOCKET CJHFaxDlg::FindBBBSocket(wchar_t *ap_bbb_name)
{
	int index = m_user_list.FindStringExact(-1, ap_bbb_name);
	if (index != LB_ERR){
		UserInformation *p_user_info = (UserInformation *)m_user_list.GetItemDataPtr(index);
		if(p_user_info->p_runtime_info != NULL) return p_user_info->p_runtime_info->h_socket;
	}

	return INVALID_SOCKET;
}

SocketData *CJHFaxDlg::FindBBBSocketData(wchar_t *ap_bbb_name)
{
	int index = m_user_list.FindStringExact(-1, ap_bbb_name);
	if (index != LB_ERR){
		UserInformation *p_user_info = (UserInformation *)m_user_list.GetItemDataPtr(index);
		if (p_user_info->p_runtime_info != NULL) return p_user_info->p_runtime_info;
	}

	return NULL;
}


// JHServerSocket 사용자 정의 메시지 프로시저
void CJHFaxDlg::UserProcessReadData(SOCKET parm_socket, unsigned char parm_message_id, unsigned int parm_data_size, void *parm_data)
{
	if (NM_CHAT_MESSAGE == parm_message_id){
		BroadCastData(parm_message_id, parm_data, parm_data_size);
		m_chat_list.InsertString(-1, (wchar_t *)parm_data);
	}
	else if (NM_LOGIN_MESSAGE == parm_message_id){
		ProcessLoginMessage(parm_socket, parm_data_size, parm_data);
	}
	else if (NM_START_SEND_FILES == parm_message_id){
		unsigned short index = 0;
		if (!FindSocketIndex(m_prime_list, m_prime_count, parm_socket, &index)) return;

		UserInformation *p_prime_info = (UserInformation *)m_prime_list[index].p_user_info;
		
		m_prime_list[index].h_favorit_socket = FindBBBSocket(p_prime_info->favorit_user);

		if (INVALID_SOCKET == m_prime_list[index].h_favorit_socket){
			SendFrameData(parm_socket, NM_CANNOT_START_SEND_FILE, NULL, 0);
		}
		else {
			// 범블비에 favorit_prime 설정
			int bbb_list_index = m_user_list.FindStringExact(-1, p_prime_info->favorit_user);
			UserInformation *p_bbb_info = (UserInformation *)m_user_list.GetItemDataPtr(bbb_list_index);
			wcscpy_s(p_bbb_info->favorit_user, p_prime_info->id);

			SendFrameData(parm_socket, NM_REQUEST_FILE, NULL, 0);
			m_ex_manager.SetPartner(m_prime_list + index, p_bbb_info->p_runtime_info);
		}

		// if (m_prime_list[index].h_favorit_socket != INVALID_SOCKET)
	}
	else if (NM_SEND_FILE_NAME == parm_message_id){
		m_ex_manager.StartSendFile(parm_socket, (char *)parm_data, parm_data_size);
	}
	else if (NM_SEND_FILE_DATA == parm_message_id){
		m_ex_manager.AddFileData(parm_socket, (char *)parm_data, parm_data_size);
	}
	else if (NM_LAST_FILE_DATA == parm_message_id){
		m_ex_manager.EndOfFileData(parm_socket, (char *)parm_data, parm_data_size);
		m_save_paper_count++;
	}
	else if (NM_END_SEND_FILE == parm_message_id){
		UserInformation *p_prime_info = m_ex_manager.EndSendFile(parm_socket);

		SetDlgItemInt(IDC_PAPERLESS_EDIT, m_save_paper_count);
		// 프라임에서 데이터를모두 전송받았음
		// 이 파일들을 범블비에게 전송해야함

		////////////////////////////////////인식//////////////////////////////////
		SYSTEMTIME prime_time;
		GetLocalTime(&prime_time);
		CString fax_list_time;

		fax_list_time.Format(L"%04d년 %02d월 %02d일 %02d시 %02d분 %02d초 : %s -> %s", 
		prime_time.wYear, prime_time.wMonth, prime_time.wDay, prime_time.wHour, prime_time.wMinute, prime_time.wSecond, p_prime_info->id, p_prime_info->favorit_user);
		AddFaxlistString(fax_list_time);
	}
	else if (NM_CLIENT_TYPE_MESSAGE == parm_message_id){
		SendMessage(LM_CLIENT_TYPE_MESSAGE, parm_socket, *(char *)parm_data);
		// 사용자 정의 메시지 추가
	}
	else if (NM_REQUEST_BBB_LIST == parm_message_id){
		Request_BBB_List(parm_socket);
	}
	else if (NM_SELECT_BBB_NAME == parm_message_id){
		for (unsigned int i = 0; i < m_prime_count; i++){
			if (parm_socket == m_prime_list[i].h_socket){
				wcscpy_s(m_prime_list[i].p_user_info->favorit_user, 32, (wchar_t *)parm_data);

				SaveUserData();
				m_user_list.Invalidate();
				break;
			}
		}
	}
	else if (NM_BBB_SCREEN_DATA == parm_message_id){
		unsigned short index = 0;
		if (!FindSocketIndex(m_bumblebee_list, m_bumblebee_count, parm_socket, &index)) return;
		memcpy(m_bumblebee_list[index].screen, parm_data, parm_data_size);

		NotifyFavoritBBBStateToPrime(parm_socket, 1, parm_data_size, parm_data);
	}
	else if (NM_BBB_START_SEND_FILES == parm_message_id){
		unsigned short index = 0;
		if (!FindSocketIndex(m_bumblebee_list, m_bumblebee_count, parm_socket, &index)) return;

		UserInformation *p_bbb_info = (UserInformation *)m_bumblebee_list[index].p_user_info;
		SocketData *p_socket_data = FindBBBSocketData(p_bbb_info->favorit_user);
		m_bumblebee_list[index].h_favorit_socket = p_socket_data->h_socket;
	//	MessageBox(L"StartSendFile");
		if (INVALID_SOCKET == m_bumblebee_list[index].h_favorit_socket) SendFrameData(parm_socket, NM_CANNOT_START_SEND_FILE, NULL, 0);
		else {
			SendFrameData(parm_socket, NM_REQUEST_FILE, NULL, 0);
			m_ex_manager.SetPartner(m_bumblebee_list + index, p_socket_data);
		}
	}


}


void CJHFaxDlg::NotifyFavoritBBBStateToPrime(SOCKET ap_bbb_socket, char a_login_info, unsigned int parm_screen_data_size, void *parm_screen_data)
{
	unsigned short index = 0;
	if (!FindSocketIndex(m_bumblebee_list, m_bumblebee_count, ap_bbb_socket, &index)) return;
	UserInformation *p_bbb_info = m_bumblebee_list[index].p_user_info, *p_prime_info;
	// 접속 유무(1) + 스크린 가로(4) + 세로(4)
	char stream[10];

	for (unsigned short i = 0; i < m_prime_count; i++){
		p_prime_info = m_prime_list[i].p_user_info;
		if (0 == strcmp((const char *)p_prime_info->favorit_user, (const char *)p_bbb_info->id)){
			stream[0] = a_login_info;
			memcpy(stream + 1, parm_screen_data, parm_screen_data_size);
			SendFrameData(m_prime_list[i].h_socket, NM_FAVORIT_BBB_DATA, stream, 10);
		}
	}
}


void CJHFaxDlg::Request_BBB_List(SOCKET parm_socket)
{
	int bbb_count = 0, count = m_user_list.GetCount();
	UserInformation *p_user_info;

	for (int i = 0; i < count; i++){
		p_user_info = (UserInformation *)m_user_list.GetItemDataPtr(i);
		if (CLIENT_BUMBLEBEE_TYPE == p_user_info->user_type) bbb_count++;
	}

	if(bbb_count > 0){
		BBB_State *p_stream = new BBB_State[bbb_count];
		BBB_State *p_pos = p_stream;

		for (int i = 0; i < count; i++){
			p_user_info = (UserInformation *)m_user_list.GetItemDataPtr(i);
			if (CLIENT_BUMBLEBEE_TYPE == p_user_info->user_type){
				// 아이디
				wcscpy_s(p_pos->name, 32, p_user_info->id);
				// 접속 유무
				p_pos->state = (NULL != p_user_info->p_runtime_info);
				if (p_pos->state){
					p_pos->screen[0] = p_user_info->p_runtime_info->screen[0];
					p_pos->screen[1] = p_user_info->p_runtime_info->screen[1];
				}
				else {
					p_pos->screen[0] = 1920;
					p_pos->screen[1] = 1080;
				}
				p_pos++;
			}
		}

		SendFrameData(parm_socket, NM_SEND_BBB_LIST, p_stream, sizeof(BBB_State)* bbb_count);
		delete[] p_stream;
	}
}

char CJHFaxDlg::ProcessLoginMessage(SOCKET parm_socket, unsigned int parm_data_size, void *parm_data)
{
	// if (parm_data_size != sizeof(LoginData)) return 0;
	LoginData *p_log_data = (LoginData *)parm_data;

	int index = m_user_list.FindStringExact(0, p_log_data->id);
	if (LB_ERR != index){
		// 서로 참조하게 해주기
		UserInformation *p_user_info = (UserInformation *)m_user_list.GetItemDataPtr(index);
		
		if (p_user_info->user_type == CLIENT_BUMBLEBEE_TYPE || 0 == wcscmp(p_user_info->password, p_log_data->pw)){// 
			SocketData*p_user_data  = TransferClientType(parm_socket, p_user_info->user_type);

			p_user_info->p_runtime_info = p_user_data;
			p_user_data->p_user_info = p_user_info;

			CString str;
			str.Format(L"%s 사용자가 로그인 하였습니다.", p_user_info->id);
			AddEventString(str);

			
			if (p_user_info->user_type == CLIENT_PRIME_TYPE) {
				// 프라임인 경우
				// 범블비 접속 정보와 스크린 데이터 보내주기 !!!!!
				BBB_State favorit_bbb_info;
				favorit_bbb_info.state = 0;
				favorit_bbb_info.screen[0] = 1920;
				favorit_bbb_info.screen[1] = 1080;

				if (p_user_info->favorit_user[0] != 0){
					// 즐찻 범블비가 존재하는 경우
					memcpy(favorit_bbb_info.name, p_user_info->favorit_user, 32);
					int bbb_index = m_user_list.FindStringExact(-1, p_user_info->favorit_user);
					// 읽는 동안 오류 발생
					if (LB_ERR == bbb_index) favorit_bbb_info.name[0] = 0;
					else {
						UserInformation *p_user_info = (UserInformation *)m_user_list.GetItemDataPtr(bbb_index);
						if (p_user_info->p_runtime_info != NULL){
							favorit_bbb_info.state = 1;
							SocketData *p_bbb_socket_data = p_user_info->p_runtime_info;
							favorit_bbb_info.screen[0] = p_bbb_socket_data->screen[0];
							favorit_bbb_info.screen[1] = p_bbb_socket_data->screen[1];
						}
					}
				} else {
					// 즐찻 범블비가 없는 경우
					favorit_bbb_info.name[0] = 0;
				}
				SendFrameData(parm_socket, NM_LOGIN_SUCCESS_MESSAGE, &favorit_bbb_info, sizeof(BBB_State));

			}
			else {
				//범블비일 경우
				SendFrameData(parm_socket, NM_LOGIN_SUCCESS_MESSAGE, NULL, 0);
			}
			return 1;
		}	
	}

	SendFrameData(parm_socket, NM_LOGIN_FAIL_MESSAGE, NULL, 0);

	return 0;
}

////////////////////////////////인식//////////////////////////////
void CJHFaxDlg::AddFaxlistString(const wchar_t *ap_string)
{
	int index = m_fax_list.InsertString(-1, ap_string);
	m_fax_list.SetCurSel(index);
}
////////////////////////////////////////////////////////////////////

void CJHFaxDlg::AddEventString(const wchar_t *ap_string)
{
	int index = m_chat_list.InsertString(-1, ap_string);
	m_chat_list.SetCurSel(index);
}

// #include "XpsViewerDlg.h"

LRESULT CJHFaxDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (LM_SEND_FILE_MESSAGE == message){
		wchar_t *p_file_name = (wchar_t *)wParam;
		delete[] p_file_name;
	}
	return JHServerSocket::WindowProc(message, wParam, lParam);
}

// afx_msg LRESULT CJHFaxDlg::OnLmClientTypeMessage(WPARAM wParam, LPARAM lParam)
SocketData*CJHFaxDlg::TransferClientType(SOCKET parm_socket, char parm_type)
{
	SocketData*p_user_data;
	// ServerSocket 에서 해당 소켓 인덱스 찾기
	unsigned short index;
	JHServerSocket::FindSocketIndex(parm_socket, &index);
	
	char temp_ip[24];
	// Arrange : 해당 소켓 ServerSocket 에서 지우기
	JHServerSocket::ArrangeSocket(index, temp_ip);

	CString str;
	// 프라임과 범블비 개수 제한 체크 
	// 사용자 소켓 리스트에 추가
	if (CLIENT_PRIME_TYPE == parm_type){
		m_prime_list[m_prime_count].h_socket = parm_socket;
		memcpy(m_prime_list[m_prime_count].ip, temp_ip, 24);

		p_user_data = m_prime_list + m_prime_count;

		m_prime_count++;
		// str.Format(L" 프라임이 접속하였습니다.");

		// favorit_user 상태 

	} else {
		m_bumblebee_list[m_bumblebee_count].h_socket = parm_socket;
		memcpy(m_bumblebee_list[m_bumblebee_count].ip, temp_ip, 24);
		
		p_user_data = m_bumblebee_list + m_bumblebee_count;

		m_bumblebee_count++;
		
	//	str.Format(L"%s 범블비가 접속하였습니다.",m_bumblebee_list[m_bumblebee_count-1].p_user_info->id);
	}
	//str.Format(L"%s 범블비가 접속하였습니다.", m_bumblebee_list[m_bumblebee_count - 1].p_user_info->id);
	//AddEventString(str);
	m_user_count++;
	SetDlgItemInt(IDC_CUR_USER_EDIT, m_user_count);
	return p_user_data;
}


BOOL CJHFaxDlg::FindSocketIndex(SocketData*parm_list, unsigned int parm_list_count, SOCKET parm_socket, unsigned short *parm_index)
{
	for (unsigned int i = 0; i < parm_list_count; i++)
	{
		if (parm_socket == parm_list[i].h_socket){
			*parm_index = i;
			return 1;
		}
	}
	return 0;
}

void CJHFaxDlg::ArrangeSocket(SocketData*parm_list, unsigned int *parm_p_list_count, unsigned short parm_socket_index)
{
	(*parm_p_list_count)--;
	int temp_index = *parm_p_list_count;
	if (temp_index != parm_socket_index){
		// memcpy 로 SocketData복사
		parm_list[parm_socket_index].h_socket = parm_list[temp_index].h_socket;
		memcpy(parm_list[parm_socket_index].ip, parm_list[temp_index].ip, 24);
	}
}

// 1 : temp 에서 처리 
char CJHFaxDlg::ProcessCloseMessage(SOCKET parm_socket)
{
	char result = JHServerSocket::ProcessCloseMessage(parm_socket);
	
	if (1 == result ) return 1; // 1 : 서버에서 찾아서 정리함
	else if (0 == result){
		// 0 : 서버에 없음
		unsigned short index;
		CString str;
		if (1 == FindSocketIndex(m_prime_list, m_prime_count, parm_socket, &index)){
			//CString ip(m_prime_list[index].ip);
			//str.Format(L"Prime : %s 가 접속을 해제하였습니다.", ip);
			str.Format(L"%s 가 로그아웃 하였습니다.", m_prime_list[index].p_user_info->id);
			
			m_prime_list[index].p_user_info->p_runtime_info = NULL;
			ArrangeSocket(m_prime_list, &m_prime_count, index);
		}
		else {
			FindSocketIndex(m_bumblebee_list, m_bumblebee_count, parm_socket, &index);
			//str.Format(L"BumBleBee : %s 가 접속을 해제하였습니다.", m_bumblebee_list[index].ip);
			str.Format(L"%s 가 로그아웃 하였습니다.", m_bumblebee_list[index].p_user_info->id);
			m_bumblebee_list[index].p_user_info->p_runtime_info = NULL;

			ArrangeSocket(m_bumblebee_list, &m_bumblebee_count, index);
			NotifyFavoritBBBStateToPrime(parm_socket, 0, 0, NULL);
		}
		m_user_count--;
		SetDlgItemInt(IDC_CUR_USER_EDIT, m_user_count);
		DestroySocket(parm_socket);
		AddEventString(str);

		return 2;
	}
	return 1;
}

#include "AddModifyUserDlg.h"

void CJHFaxDlg::SaveUserData()
{
	int count = m_user_list.GetCount();

	CFile save_file;
	if (TRUE == save_file.Open(L"user.dat", CFile::modeCreate | CFile::modeWrite)){
		save_file.Write(&count, sizeof(int));

		for (int i = 0; i < count; i++){
			save_file.Write(m_user_list.GetItemDataPtr(i), sizeof(UserInformation));
		}

		save_file.Close();
	}

}

void CJHFaxDlg::LoadUserData()
{
	int count = 0;

	CFile load_file;
	if (TRUE == load_file.Open(L"user.dat", CFile::modeRead)){
		load_file.Read(&count, sizeof(int));

		UserInformation *p_data;
		for (int i = 0; i < count; i++){
			p_data = new UserInformation;
			// p_data->
			load_file.Read(p_data, sizeof(UserInformation));
			// 초기화
			p_data->p_runtime_info = NULL;

			m_user_list.InsertString(i, p_data->id);
			m_user_list.SetItemDataPtr(i, p_data);
		}

		load_file.Close();
	}
}

void CJHFaxDlg::OnBnClickedAddUserBtn()
{
	AddModifyUserDlg ins_dlg;

	if (IDOK == ins_dlg.DoModal()){
		UserInformation *p_data = new UserInformation;

		wcscpy_s(p_data->id, ins_dlg.GetUserID().GetLength() + 1, ins_dlg.GetUserID());
		wcscpy_s(p_data->password, ins_dlg.GetUserPassword().GetLength() + 1, ins_dlg.GetUserPassword());
		p_data->user_type = ins_dlg.GetType();
		p_data->page_count = 0;
		p_data->favorit_user[0] = 0;

		CreateDirectory(L"png\\" + CString(p_data->id), NULL);

		int index = m_user_list.AddString(p_data->id);
		m_user_list.SetItemDataPtr(index, p_data); 

		SaveUserData();
	}
}


void CJHFaxDlg::OnBnClickedModifyUserBtn()
{
	int index = m_user_list.GetCurSel();
	if (index != LB_ERR){
		UserInformation *p_data = (UserInformation *)m_user_list.GetItemDataPtr(index);
		AddModifyUserDlg ins_dlg;

		ins_dlg.SetUserID(p_data->id);
		ins_dlg.SetUserPassword(p_data->password);
		ins_dlg.SetType(p_data->user_type);

		if (IDOK == ins_dlg.DoModal()){
			wcscpy_s(p_data->password, ins_dlg.GetUserPassword().GetLength() + 1, ins_dlg.GetUserPassword());
			p_data->user_type = ins_dlg.GetType();

			CreateDirectory(L"png\\" + CString(p_data->id), NULL);

			SaveUserData();
			m_user_list.SetCurSel(index);
		}
	}

}


void CJHFaxDlg::OnBnClickedDelUserBtn()
{
	int index = m_user_list.GetCurSel();
	if (LB_ERR != index){
		UserInformation *p_data = (UserInformation *)m_user_list.GetItemDataPtr(index);
		if (IDOK == MessageBox(p_data->id, L"아래의 사용자를 삭제하시겠습니까?", MB_OKCANCEL | MB_ICONQUESTION)){
			delete p_data;
			m_user_list.DeleteString(index);
			SaveUserData();
		}
	}
}


void CJHFaxDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (nIDCtl == IDC_USER_LIST){
		int index = lpDrawItemStruct->itemID;
		if (index >= 0 && index < m_user_list.GetCount()){
			UserInformation *p_data = (UserInformation *)m_user_list.GetItemDataPtr(index);

			CDC *p_dc = CDC::FromHandle(lpDrawItemStruct->hDC);
			CRect r(lpDrawItemStruct->rcItem);

			if (lpDrawItemStruct->itemState & ODS_SELECTED){
				p_dc->FillSolidRect(r, RGB(0, 255, 0));
			}
			else {
				p_dc->FillSolidRect(r, RGB(255, 255, 255));
			}			

			p_dc->TextOut(r.left + 5, r.top + 2, p_data->id);
			p_dc->TextOut(r.left + 100, r.top + 2, p_data->password);
			if (p_data->user_type == 0) p_dc->TextOut(r.left + 200, r.top + 2, L"프라임");
			else p_dc->TextOut(r.left + 200, r.top + 2, L"범블비");

			p_dc->TextOut(r.left + 260, r.top + 2, p_data->favorit_user);
		}

		return;
	}

	JHServerSocket::OnDrawItem(nIDCtl, lpDrawItemStruct);
}


void CJHFaxDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (nIDCtl == IDC_USER_LIST){
		lpMeasureItemStruct->itemHeight = 20;
		return;
	}

	JHServerSocket::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}



void CJHFaxDlg::OnBnClickedOk()
{
	if (IDOK == MessageBox(L"종료시 사용자들의 정보 및 팩스 내용은 자동 저장됩니다", L"종이를 아끼는 시스템, Faxcent 를 종료하시겠습니까?", MB_OKCANCEL | MB_ICONQUESTION)){
		// 저장되는 프로세스 보여주는거 있으면 좋겠다!!! 타이머 돌면서 프로그래스바 막 나타나고 ㅎㅎㅎ
		OnOK();
	}
}


void CJHFaxDlg::OnEnChangeIpEdit()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// JHServerSocket::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}