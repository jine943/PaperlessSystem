#include "stdafx.h"
#include "JHServerSocket.h"



JHServerSocket::JHServerSocket(UINT nIDTemplate, CWnd* pParentWnd) : CDialog(nIDTemplate, pParentWnd)
{
	WSADATA data;
	// 소켓 버전, 가져온 정보 저장하는 구조체 변수 
	WSAStartup(0x0202, &data);
	mh_listen_socket = INVALID_SOCKET;
	m_client_count = 0;
	mp_list_box = NULL;
}

void JHServerSocket::SetSocketData(char *parm_address, unsigned short parm_port)
{
	strcpy_s(m_ip_address, parm_address);
	m_port = parm_port;
}

void JHServerSocket::StartListenSocket()
{
	struct sockaddr_in srv_addr;
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr(m_ip_address);
	srv_addr.sin_port = htons(m_port);

	mh_listen_socket = socket(AF_INET, SOCK_STREAM, 0);

	bind(mh_listen_socket, (LPSOCKADDR)&srv_addr, sizeof(struct sockaddr_in));

	listen(mh_listen_socket, 1);
	WSAAsyncSelect(mh_listen_socket, m_hWnd, LM_SOCKET_MESSAGE, FD_ACCEPT);
}


BEGIN_MESSAGE_MAP(JHServerSocket, CDialog)
	ON_MESSAGE(LM_SOCKET_MESSAGE, OnSocketProcess)
END_MESSAGE_MAP()


LRESULT JHServerSocket::OnSocketProcess(WPARAM wParam, LPARAM lParam)
{
	SOCKET cur_socket = (SOCKET)wParam;

	int event_type = WSAGETSELECTEVENT(lParam);

	if (event_type == FD_READ){
		ProcessReadMessage(cur_socket);
	}
	else if (event_type == FD_ACCEPT){
		if (0 == WSAGETSELECTERROR(lParam)){
			ProcessAcceptMessage(cur_socket);
		}
	}
	else if (event_type == FD_CLOSE){
		// 재정의
		ProcessCloseMessage(cur_socket);
	}
	else return 0;

	return 1;
}


// FD_ACCEPT 발생시 
char JHServerSocket::ProcessAcceptMessage(SOCKET parm_socket)
{
	if (m_client_count < MAX_CLIENT_COUNT){
		struct sockaddr_in client_addr;
		int client_addr_size = sizeof(struct sockaddr_in);
		mh_client_socket[m_client_count] = accept(parm_socket, (LPSOCKADDR)&client_addr, &client_addr_size);
		 
		if (0 == WSAAsyncSelect(mh_client_socket[m_client_count], m_hWnd, LM_SOCKET_MESSAGE, FD_READ | FD_CLOSE)){
			memcpy(mh_client_ip[m_client_count], inet_ntoa(client_addr.sin_addr), 24);

			if (NULL != mp_list_box){
				CString ip_str(mh_client_ip[m_client_count]);
				CString str;
				str.Format(L"IP : %s 가 접속했습니다.", ip_str);
				AddEventString(str);
			}
			m_client_count++;
		} // 비동기 실패
		else return -3;
	}
	else {
		// 더이상 클라이언트 접속 못함
		return -1;
	}
	return 1;
}

void JHServerSocket::AddEventString(const wchar_t *ap_string)
{
	int index = mp_list_box->InsertString(-1, ap_string);
	mp_list_box->SetCurSel(index);
}


// Get 원본 데이터를 참조
// Read 데이터 자체를 가져오기 뺏어오기 내가 가져오면 상대의 것은 사라지는  

char JHServerSocket::ReadBodyData(SOCKET parm_socket, int parm_size, char *parm_body_data)
{
	if (parm_body_data != NULL) {
		char retry_count = 0;
		int total_read_size = 0, read_size = 0;

		while (total_read_size < parm_size){
			read_size = recv(parm_socket, parm_body_data + total_read_size, parm_size - total_read_size, 0);
			if (0 == read_size || SOCKET_ERROR == read_size){
				retry_count++;
				if (retry_count >= 500) {
					return NULL;
				}
				else Sleep(50); // 데이터가 안들어올 때 너무 오래 쉬는거야
			}
			else{
				total_read_size += read_size;
				retry_count = 0;
			}
		} // 데이터를 읽지 못한 경우
		return 1;

	}
	return 0;
}



int JHServerSocket::SendFrameData(SOCKET parm_socket, unsigned char parm_message_id, void *parm_data, int parm_send_size)
{
	char *p_send_data = new char[1 + 1 + 4 + parm_send_size];
	p_send_data[0] = KEY_VALUE;
	p_send_data[1] = parm_message_id;
	*(int *)(p_send_data + 2) = parm_send_size;

	if (0 != parm_send_size){
		// strcpy : 문자열 카피, 중간에 NULL 이 나오면 그만 읽는 함수, 아스키 카피 방식
		// 바이너리 카피 더 빠르다
		memcpy(p_send_data + 6, parm_data, parm_send_size);
	}

	char retry_count = 0;
	int total_send_size = 0, real_send_size = 0, send_size = 6 + parm_send_size;

	while (total_send_size < send_size){
		real_send_size = send(parm_socket, p_send_data + total_send_size, send_size - total_send_size, 0);
		if (0 == real_send_size || SOCKET_ERROR == real_send_size){
			retry_count++;
			if (retry_count >= 500) {
				return NULL;
			}
			else Sleep(50);
		}
		else{
			total_send_size += real_send_size;
			retry_count = 0;
		}
	}

	delete[] p_send_data;
	return send_size;
}


// 모든 클라이언트에게 데이터를 전송하는 함수
char JHServerSocket::BroadCastData(unsigned char parm_message_id, void *parm_data, int parm_data_size)
{
	for (unsigned int i = 0; i < m_client_count; i++){
		SendFrameData(mh_client_socket[i], parm_message_id, parm_data, parm_data_size);
	}
	return 1;
}

// FD_READ 발생시 메시지 처리기
char JHServerSocket::ProcessReadMessage(SOCKET parm_socket)
{
	char result = WSAAsyncSelect(parm_socket, m_hWnd, LM_SOCKET_MESSAGE, FD_CLOSE);
	if (result == 0){
		char key; 
		recv(parm_socket, &key, 1, 0);
			if (KEY_VALUE == key){
				unsigned char message_id;
				if (recv(parm_socket, (char *)&message_id, 1, 0)){
					// data 길이
					unsigned int data_size = 0;
					if (recv(parm_socket, (char *)&data_size, 4, 0)){
						if (0 < data_size){
							char *p_data = new char[data_size];
							if (ReadBodyData(parm_socket, data_size, p_data)){
								UserProcessReadData(parm_socket, message_id, data_size, p_data);
							}
							else {
								ProcessCloseMessage(parm_socket);
							}
							delete[] p_data;
						}
						else {
							UserProcessReadData(parm_socket, message_id, NULL, 0);
						}
						if (0 != WSAAsyncSelect(parm_socket, m_hWnd, LM_SOCKET_MESSAGE, FD_CLOSE | FD_READ)){
							// FD_CLOSE | FD_READ 비동기에 실패한 경우
							return -7;
						}
					} // 데이터 사이즈를 얻지 못한 경우
					else return -5;

				} // 메시지 아이디 못 받은 경우
				else return -3;
			}
			else {
				ProcessCloseMessage(parm_socket);
				// 잘못된 key 값을 전송한 클라이언트의 경우
				return -4;
			}
	} // fd_close 비동기에 실패한 경우
	else return -1;

	return 1;
}


// Process
// 반환값 -1 : 에러, 1 : 성공, 0 : 못찾음
// FD_CLOSE 발생시 메시지 처리기
char JHServerSocket::ProcessCloseMessage(SOCKET parm_socket)
{
	if (INVALID_SOCKET == parm_socket) return -1;
	
	char result = 0;
	unsigned short index;

	if (FindSocketIndex(parm_socket, &index)){
		DestroySocket(parm_socket);
		ArrangeSocket(index);

		if (NULL != mp_list_box){
			CString str;
			CString ip_str(mh_client_ip[index]);
			str.Format(L"IP : %s 에서 접속을 해제하였습니다.", ip_str);
			mp_list_box->InsertString(-1, str);
		}
		return 1;
	}
	return 0;
}

BOOL JHServerSocket::FindSocketIndex(SOCKET parm_socket, unsigned short *parm_index)
{
	for (unsigned int i = 0; i < m_client_count; i++)
	{
		if (parm_socket == mh_client_socket[i]){
			*parm_index = i;
			return 1;
		}
	}
	return 0;
}

void JHServerSocket::ArrangeSocket(unsigned short parm_socket_index, char *parm_ip)
{
	if (--m_client_count != parm_socket_index){
		mh_client_socket[parm_socket_index] = mh_client_socket[m_client_count];
		if (NULL != parm_ip) memcpy(parm_ip, mh_client_ip[parm_socket_index], 24);
		memcpy(mh_client_ip[parm_socket_index], mh_client_ip[m_client_count], 24);
	}
}

// 소켓 제거시 linger 옵션 없이 제거하면 데이터가 전송중일 경우 끊어지면서 바로 종료되지 않는다.
// 즉시 종료하기 위해서 제거할 소켓에 링거 옵션 걸어주기 위한 것
char JHServerSocket::DestroySocket(SOCKET parm_socket)
{
	LINGER l_linger = { TRUE, 0 };
	setsockopt(parm_socket, SOL_SOCKET, SO_LINGER, (char FAR *)&l_linger, sizeof(l_linger));
	closesocket(parm_socket);
	return 1;
}


JHServerSocket::~JHServerSocket()
{
	DestroySocket(mh_listen_socket);

	for (unsigned int i = 0; i < m_client_count; i++){
		if (INVALID_SOCKET != mh_client_socket[i]) DestroySocket(mh_client_socket[i]);
	}
}

