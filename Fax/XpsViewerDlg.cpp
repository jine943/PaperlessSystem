// XpsViewerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JHFax.h"
#include "XpsViewerDlg.h"
#include "afxdialogex.h"


// XpsViewerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(XpsViewerDlg, CDialogEx)

XpsViewerDlg::XpsViewerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(XpsViewerDlg::IDD, pParent)
{
	m_step_count = 0;
}

XpsViewerDlg::~XpsViewerDlg()
{
}

void XpsViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(XpsViewerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// XpsViewerDlg 메시지 처리기입니다.


void XpsViewerDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect r(0, 0, m_xps_image.GetWidth(), m_xps_image.GetHeight());

	m_xps_image.Draw(dc.GetSafeHdc(), r);
}


BOOL XpsViewerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	MoveWindow(CRect(m_xps_image.GetWidth()*(-1), 0, 0, m_xps_image.GetHeight()));

	SetTimer(1, 50, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void XpsViewerDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 1){
		MoveWindow(CRect(m_xps_image.GetWidth()*(-1) + m_step_count, 0, m_step_count, m_xps_image.GetHeight()));

		if (m_step_count < m_xps_image.GetWidth()) m_step_count += 20;
		else KillTimer(1);
	}

	CDialogEx::OnTimer(nIDEvent);
}
