#ifndef _EXCHANGE_SERVICE_H_
#define _EXCHANGE_SERVICE_H_

struct SocketData;
class JHServerSocket;

#define MAX_EX_COUNT     50

class ExchangeService
{
private:
	SocketData *mp_sender[MAX_EX_COUNT];
	SocketData *mp_receiver[MAX_EX_COUNT];
	CFile *mp_file[MAX_EX_COUNT];

	int m_ex_count;
	JHServerSocket *mp_socket;

public:
	ExchangeService();
	~ExchangeService();

	void SetSocketInfo(JHServerSocket *ap_socket);
	int SetPartner(SocketData *ap_sender, SocketData *ap_receiver);

	void StartSendFile(SOCKET ah_socket, char *ap_data, unsigned int a_data_size);
	void AddFileData(SOCKET ah_socket, char *ap_data, unsigned int a_data_size);
	void EndOfFileData(SOCKET ah_socket, char *ap_data, unsigned int a_data_size);
	UserInformation *EndSendFile(SOCKET ah_socket);
};


#endif