#include "stdafx.h"

#include "JHServerSocket.h"
#include "ExchangeService.h"

ExchangeService::ExchangeService()
{
	m_ex_count = 0;
	mp_socket = NULL;

	for (int i = 0; i < MAX_EX_COUNT; i++) mp_file[i] = NULL;
}

ExchangeService::~ExchangeService()
{
	for (int i = 0; i < m_ex_count; i++) {
		if (mp_file[i] != NULL){
			mp_file[i]->Close();
			delete mp_file[i];
		}
	}

}

int ExchangeService::SetPartner(SocketData *ap_sender, SocketData *ap_receiver)
{
	if (m_ex_count < MAX_EX_COUNT){
		mp_sender[m_ex_count] = ap_sender;
		mp_receiver[m_ex_count] = ap_receiver;

		m_ex_count++;
		return 1;
	}

	return 0;
}

void ExchangeService::SetSocketInfo(JHServerSocket *ap_socket)
{
	mp_socket = ap_socket;
}

void ExchangeService::StartSendFile(SOCKET ah_socket, char *ap_data, unsigned int a_data_size)
{
	int index;
	for (index = 0; index < m_ex_count; index++){
		if (mp_sender[index]->h_socket == ah_socket) break;
	}

	if (index >= m_ex_count) return;

	CString file_path((wchar_t *)ap_data);
	file_path.Insert(6, mp_sender[index]->p_user_info->favorit_user + CString("\\"));

	mp_file[index] = new CFile;
	mp_file[index]->Open(file_path, CFile::modeCreate | CFile::modeWrite);

	if (mp_receiver[index]->h_socket != INVALID_SOCKET){
		mp_socket->SendFrameData(mp_receiver[index]->h_socket, NM_SEND_FILE_NAME, ap_data, a_data_size);
	}

	mp_socket->SendFrameData(mp_sender[index]->h_socket, NM_NEXT_FILE_DATA, NULL, 0);
}


void ExchangeService::AddFileData(SOCKET ah_socket, char *ap_data, unsigned int a_data_size)
{
	int index;
	for (index = 0; index < m_ex_count; index++){
		if (mp_sender[index]->h_socket == ah_socket) break;
	}

	if (index >= m_ex_count) return;

	mp_file[index]->Write(ap_data, a_data_size);

	if (mp_receiver[index]->h_socket != INVALID_SOCKET){
		mp_socket->SendFrameData(mp_receiver[index]->h_socket, NM_SEND_FILE_DATA, ap_data, a_data_size);
	}

	mp_socket->SendFrameData(mp_sender[index]->h_socket, NM_NEXT_FILE_DATA, NULL, 0);
}

void ExchangeService::EndOfFileData(SOCKET ah_socket, char *ap_data, unsigned int a_data_size)
{
	int index;
	for (index = 0; index < m_ex_count; index++){
		if (mp_sender[index]->h_socket == ah_socket) break;
	}

	if (index >= m_ex_count) return;

	mp_file[index]->Write(ap_data, a_data_size);
	mp_file[index]->Close();
	delete mp_file[index];
	mp_file[index] = NULL;

	if (mp_receiver[index]->h_socket != INVALID_SOCKET){
		mp_socket->SendFrameData(mp_receiver[index]->h_socket, NM_LAST_FILE_DATA, ap_data, a_data_size);
	}

	mp_socket->SendFrameData(mp_sender[index]->h_socket, NM_REQUEST_FILE, NULL, 0);
}

UserInformation *ExchangeService::EndSendFile(SOCKET ah_socket)
{
	int index;
	for (index = 0; index < m_ex_count; index++){
		if (mp_sender[index]->h_socket == ah_socket) break;
	}

	if (index >= m_ex_count) return NULL;

	UserInformation *p_user_info = mp_sender[index]->p_user_info;

	if (mp_receiver[index]->h_socket != INVALID_SOCKET){
		mp_socket->SendFrameData(mp_receiver[index]->h_socket, NM_END_SEND_FILE, NULL, 0);
	}

	m_ex_count--;
	if (index < m_ex_count){
		mp_sender[index] = mp_sender[m_ex_count];
		mp_receiver[index] = mp_receiver[m_ex_count];
		mp_file[index] = mp_file[m_ex_count];
	}
	return p_user_info;
}