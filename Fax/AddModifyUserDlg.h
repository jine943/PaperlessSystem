#pragma once


// AddModifyUserDlg 대화 상자입니다.

class AddModifyUserDlg : public CDialogEx
{
	DECLARE_DYNAMIC(AddModifyUserDlg)
private:
	CString m_user_id, m_user_password;
	char m_type;

public:
	AddModifyUserDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~AddModifyUserDlg();

	void SetUserID(CString parm_id_string)
	{
		m_user_id = parm_id_string;
	}

	CString GetUserID()
	{
		return m_user_id;
	}

	void SetUserPassword(CString parm_password_string)
	{
		m_user_password = parm_password_string;
	}

	CString GetUserPassword()
	{
		return m_user_password;
	}

	void SetType(char parm_type)
	{
		m_type = parm_type;
	}

	char GetType()
	{
		return m_type;
	}

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ADD_MODIFY_USER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
